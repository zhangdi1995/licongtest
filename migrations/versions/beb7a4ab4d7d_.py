"""empty message

Revision ID: beb7a4ab4d7d
Revises: 8bc93d5d6a2c
Create Date: 2016-11-07 16:16:42.337164

"""

# revision identifiers, used by Alembic.
revision = 'beb7a4ab4d7d'
down_revision = '8bc93d5d6a2c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('posts', sa.Column('body_html', sa.Text(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('posts', 'body_html')
    ### end Alembic commands ###
