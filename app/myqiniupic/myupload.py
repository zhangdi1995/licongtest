# -*- coding: utf-8 -*-
# flake8: noqa

from qiniu import Auth, put_file, etag, urlsafe_base64_encode
import qiniu.config
import requests
import os
from app import host
# host = 'http://123.207.235.79:3000'
# host="http://123.206.231.112:3000"#api
# host='https://dev.api.kquestions.com'
#需要填写你的 Access Key 和 Secret Key
access_key = '1WCJfXWbaq4IQD9rhsgF5Mi-JCfh79xsUJUg3n5s'
secret_key = '1AJcUomeRSECeDxhbArWyqhdha_ve2gy7Px1SGrL'

#构建鉴权对象
q = Auth(access_key, secret_key)

#要上传的空间
bucket_name = 'test'
def upload(name,path):
    #上传到七牛后保存的文件名
    key = name

    #生成上传 Token，可以指定过期时间等
    if key.endswith('.html') or key.startswith('htmlcontent_'):
        res_name = {"res_name": key,"public" : True}
    else:
        res_name = {"res_name": key,"public" : False}
    tokenresp = requests.post(host+'/res/token',json=res_name,verify=True).json()
    print tokenresp
    token = tokenresp['uptoken']

    #要上传文件的本地路径
    localfile = path

    ret, info = put_file(token, key, localfile)
    print(info)
    assert ret['key'] == key
    assert ret['hash'] == etag(localfile)
    os.remove(path)

