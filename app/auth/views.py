# -*- coding:utf-8 -*-
import warnings
warnings.filterwarnings("ignore")
from flask import render_template, redirect, request, url_for, flash, make_response,session
from . import auth
import uuid
from .forms import LoginForm,RegistrationForm,editpasswordForm
from flask.ext.login import login_user
from ..models import User
from flask.ext.login import logout_user, login_required
from .. import db
from flask.ext.login import current_user
import requests,json
import redis,json
import jwt,time
from app import host
# host="http://123.207.235.79:3000"
# host="http://123.206.231.112:3000"#api
# host='http://192.168.1.22:3000'
# host='https://dev.api.kquestions.com'
r = redis.Redis(port=6380) #todo:when push , turn it to 6380
# host="https://api.kquestions.com"
@auth.route('/adminlogin', methods=['GET', 'POST'])
def adminlogin():
    form = request.get_json()
    if form:
        data={ "admin" : { "email" : form['username'], "password" : form['password'] } }
        a=requests.post(host+'/admin/login',json=data)
        if request.headers.getlist("X-Real-IP"):
           ip = request.headers.getlist("X-Real-IP")[0]
        else:
           ip = request.remote_addr
        print a.json()


        if 'access_token' in a.json().keys():

            session['adminid'] = a.json()['admin']['id']
            session['adminlevel']=a.json()['admin']['level']
            print session['adminid']
            r.hset('admin',session['adminid'],json.dumps(a.json()))
            r.hset('ip',session['adminid'],ip)

            return json.dumps({'ok':1})



        # user = User.query.filter_by(email=form.email.data).first()
        # if user is not None and user.verify_password(form.password.data):
        #     login_user(user, form.remember_me.data)
        #     return redirect(request.args.get('next') or url_for('main.admin'))
        resp=make_response(json.dumps({'false':0}))
        resp.status_code=403
        print 'session has key?',session.has_key('adminid')
        return resp
    else:
        return redirect(url_for('main.index'))

@auth.route('/editpassword', methods=['GET', 'POST'])
def editpassword():
    form = editpasswordForm()
    if form.validate_on_submit():
        data={ "admin" : { "password" : form.password.data, "new_password" : form.newpassword.data } }
        id = str(request.cookies.get('adminid'))
        a=requests.post(host+'/admins/'+id+'/update_password',json=data, verify=True)
        if 'id' in a.json().keys():
            flash(u'密码修改成功')
            return redirect(url_for('main.courses',exhibit='column',column=-1))
        # user = User.query.filter_by(email=form.email.data).first()
        # if user is not None and user.verify_password(form.password.data):
        #     login_user(user, form.remember_me.data)
        #     return redirect(request.args.get('next') or url_for('main.admin'))
        flash(u'原密码错误')
    return render_template('auth/login.html',form=form)


@auth.route('/logout')
# @login_required #required user has log in
def logout():
    session.pop('adminid',None)
    return redirect(url_for('main.index'))

@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()
        # if not current_user.confirmed \
        #         and request.endpoint[:5] != 'auth.':
        #     return redirect(url_for('auth.unconfirmed'))




