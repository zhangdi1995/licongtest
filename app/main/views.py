# -*- coding:utf-8 -*-
import warnings

warnings.filterwarnings("ignore")
from datetime import datetime
from flask import render_template, session, redirect, url_for, abort, flash, request, current_app, \
    make_response, jsonify, send_file
from . import main
from .forms import NameForm, EditProfileForm, EditProfileAdminForm, PostForm, CommentForm, addclass, addchapter, \
    addlesson, addbutton, addbutton2, addbutton3 \
    , editclass, editchapter, editlesson, editart_program, editart_section, editart_content, addart_program, \
    addart_section, addart_content, addvideo, adduser, edituser \
    , addcomment, editcomment, addtag, edittag, addconsumer, editconsumer
from .. import db
from ..models import User, Role, Permission, Post, Comment
from flask.ext.login import logout_user, login_required
from flask.ext.login import current_user
from ..decorators import admin_required, permission_required
import requests, json
from ..myqiniupic import myupload, mydelete
import os, uuid, base64
from PIL import Image
import re, urllib
import hashlib, time
import functools, copy
from app import host
# host = "http://123.207.235.79:3000"
# host="http://123.206.231.112:3000" #api.kq
# host='https://dev.api.kquestions.com'

# host='http://192.168.1.22:3000'
# host="https://api.pillowkquestions.com"
#
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import redis, json

r = redis.Redis(port=6380)  # todo:when push , turn it to 6380
mm = copy.deepcopy(requests.post)


def addheader(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        if session.has_key('adminid'):
            admin_token = json.loads(r.hget('admin', session['adminid']))
            ip=r.hget('ip', session['adminid'])

            if time.time() > admin_token['access_exp']:
                data = {"token": {"refresh_token": admin_token['refresh_token']}}
                resp_token = mm(host + '/admin/refresh', json=data).json()

                admin_token['access_token'], admin_token['access_exp'] = (
                resp_token['access_token'], resp_token['access_exp'])
                r.hset('admin', session['adminid'], json.dumps(admin_token))
            headers = {'Authorization': 'Bearer ' + admin_token['access_token'],
                       'CLIENT-IP':ip}
            print headers
            kw['headers'] = headers
        begin_time=time.time()
        resp=func(*args, **kw)

        print '此次请求',args,'耗时:',time.time()-begin_time

        if(resp.status_code==403):
            abort(403)
        return resp

    return wrapper


def htmltoname(s):
    if s.endswith('.html'):

        nameandformat = re.match('https:\/\/content\.kquestions\.com\/(.*)', s).group(1)
        # print nameandformat
        return nameandformat
    else:
        nameandformat = re.match('https:\/\/static\.kquestions\.com\/(.*?)\?', s).group(1)
        return nameandformat.rsplit('-', 1)[0]


@main.route('/', methods=['GET', 'POST'])
def index():
    if session.has_key('adminid') and time.time() < json.loads(r.hget('admin', session['adminid']))['refresh_exp']:

        requests.get = addheader(requests.get)
        requests.post = addheader(requests.post)

        # session['adminlevel']=requests.get(host+'/admins/'+str(session['adminid'])).json()['level']
        return redirect(url_for('main.courses',exhibit='column',column=-1))
    print 'here'
    return render_template('index.html')


# @main.route('/admin')
# def admin():
#     users_trigger=False
#     roles_trigger=False
#     users_trigger = bool(request.cookies.get('users_trigger', ''))
#     roles_trigger = bool(request.cookies.get('roles_trigger', ''))
#     return render_template('admin.html',users_trigger=users_trigger,roles_trigger=roles_trigger)
# @main.route('/admin/users')
# def admin_users():
#     resp=make_response(redirect(url_for('.admin')))
#     resp.set_cookie('users_trigger','1')
#     resp.set_cookie('roles_trigger','')
#     return resp
# @main.route('/admin/roles')
# def admin_roles():
#     resp = make_response(redirect(url_for('.admin')))
#     resp.set_cookie('users_trigger', '')
#     resp.set_cookie('roles_trigger', '1')
#     return resp



# noinspection PyPackageRequirements
def baolikey(vid):
    md5 = hashlib.md5()
    temptime = str(int(time.time()) * 1000)
    secretkey = 'dO3eMfJ1Nq'
    vidtemp = vid
    md5.update(secretkey + vidtemp[4:] + temptime)
    md5key = md5.hexdigest()
    return temptime, md5key


@main.route('/baoli', methods=['GET', 'POST'])
def baoli():
    vid = request.get_data()
    ts = baolikey(vid)[0]
    sign = baolikey(vid)[1]
    res = make_response(json.dumps({"vid": vid[4:], "ts": ts, "sign": sign}))
    res.mimetype = 'application/json'
    res.headers['Access-Control-Allow-Origin'] = '*'
    res.headers['Access-Control-Allow-Headers'] = 'X-Requested-With,X_Requested_With'
    # print 'return'
    return res


@main.route('/baoliapi', methods=['GET', 'POST'])
def baoliapi():
    import hashlib
    baoli_pure = request.get_json()
    # print baoli_pure
    sha1 = hashlib.sha1()
    writetoken = "4eb2be96-fed2-4728-a94d-d8b62ca36670"
    secretkey = 'dO3eMfJ1Nq'
    data = 'cataid=&JSONRPC=' + baoli_pure['JSONRPC'].encode('utf-8') + '&writetoken=' + writetoken + secretkey
    sha1.update(data)
    sign = sha1.hexdigest()
    return_data = json.dumps({"sign": sign, "writetoken": writetoken})
    res = make_response(return_data)
    res.mimetype = 'application/json'
    res.headers['Access-Control-Allow-Origin'] = '*'
    res.headers['Access-Control-Allow-Headers'] = 'X-Requested-With,X_Requested_With'
    # print return_data
    return res


@main.route('/articles/<exhibit>', methods=['POST', 'GET'])
def articles(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    contentindex = request.form.get('contentindex')
    sectionindex = request.form.get('sectionindex')
    programindex = request.form.get('programindex')
    indexid = request.form.get('id')

    delete = request.args.getlist('delete')
    program = request.args.get('program', -1, type=int)
    section = request.args.get('section', -1, type=int)
    content = request.args.get('content', -1, type=int)
    contents = request.args.get('contents', -1, type=int)
    edit = request.args.getlist('edit')
    if indexid and contentindex:
        a = requests.post(host + '/contents/' + str(indexid) + '/update',
                          json={'index': contentindex, 'section_id': section})
    if indexid and sectionindex:
        a = requests.post(host + '/sections/' + str(indexid) + '/update', json={'index': sectionindex})
    if indexid and programindex:
        a = requests.post(host + '/projects/' + str(indexid) + '/update', json={'index': programindex})
        # print a.json(), 1111
    videoresp = requests.get(host + '/videos?per=0', verify=True)
    videos = [i for i in videoresp.json()['records']]
    belongproject = requests.get(host + '/projects?per=0', verify=True).json()['records']
    belongsection = requests.get(host + '/sections?per=0', verify=True).json()['records']

    # ---------------get all programs(projects)----------------#
    a = requests.get(host + '/projects', verify=True)
    sections = None
    programs = [[i['name'], i['id'], 'program', i['icon'], i['banner'], i['brief'], i['index']] for i in
                a.json()['records']]

    # ---------------get all  sections----------------#
    if program != -1:
        id = str(program)
        d = requests.get(host + '/projects/' + id + '/sections', verify=True)
        sections = [[i['name'], i['id'], 'section', i['icon'], i['banner'], i['desc'], i['index']] for i in
                    d.json()['records']]

    # ---------------get all contents----------------#
    if section != -1:
        id = str(section)
        d = requests.get(host + '/sections/' + id + '/contents?per=0', verify=True)
        contents = [[i['name'], i['id'], 'content', i['ctype'], i['url'], i['banner'], i['brief'], i['index']] for i in
                    d.json()['records'] if i['section_id'] == section]

    # ---------------show all lessons----------------#
    if exhibit == 'content':
        id = '1'
        d = requests.get(host + '/contents', verify=True)
        contents = [[i['name'], i['id'], 'content', i['ctype'], i['url'], i['banner'], i['brief'], i['index']] for i in
                    d.json()['records']]

    if edit and edit[2] == 'content':
        editresp = requests.get(host + '/contents/' + str(edit[1]), verify=True).json()

        programeditresp = None
    elif edit and edit[2] == 'program' and exhibit == 'editart_program':
        programeditresp = requests.get(host + '/projects/' + str(edit[1]), verify=True).json()

        editresp = None
    else:
        editresp = None
        programeditresp = None
    tagresp = requests.get(host + '/tags?per=0', verify=True).json()['records']
    # ---------------add program(project)----------------#
    form1 = addart_program()
    # if form1.submit.data and form1.validate_on_submit():
    #
    #
    #     uid = str(uuid.uuid1())
    #     iconurl="http://oh8c4fk40.bkt.clouddn.com/"+"programicon_"+uid
    #     bannerurl="http://oh8c4fk40.bkt.clouddn.com/"+"programbanner_"+uid
    #     if form1.icon.data:
    #         iconfile = form1.icon.data.save('app/static/icons/' + form1.text.data + '.ico')
    #         myupload.upload("programicon_" + uid, 'app/static/icons/' + form1.text.data + '.ico')
    #     if form1.banner.data:
    #         bannerfile = form1.banner.data.save('app/static/banners/' + form1.text.data + '.ico')
    #         myupload.upload("programbanner_" + uid, 'app/static/banners/' + form1.text.data + '.ico')
    #     data = { "project" : { "name" : form1.text.data, "icon" : iconurl if form1.icon.data else "none" , "banner" : bannerurl if form1.banner.data else "none"} }
    #
    #     b = requests.post(host+"/projects/create", json=data,verify=True)
    #     return redirect(url_for('.articles', exhibit='program',program=-1))

    # ---------------add  section----------------#
    form2 = addart_section()
    if form2.submit2.data and form2.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionbanner_" + uid + '.jpg'
        if form2.icon2.data:
            iconfile = form2.icon2.data.save('app/static/icons/' + form2.text2.data + '.ico')
            myupload.upload("sectionicon_" + uid, 'app/static/icons/' + form2.text2.data + '.ico')
        if form2.banner2.data:
            data = {"section": {"name": form2.text2.data,
                                "banner": "uploading_" + uid + '.jpg', "project_id": program,
                                "desc": form2.desc.data}}
            c = requests.post(host + "/sections/create", json=data, verify=True)
            if 'id' in c.json().keys():
                sectionid = c.json()['id']
                bannerfile = form2.banner2.data.save('app/static/banners/' + form2.text2.data + '.jpg')
                print "sectionbanner_" + uid + '.jpg'
                myupload.upload("sectionbanner_" + uid + '.jpg', 'app/static/banners/' + form2.text2.data + '.jpg')

                data = {"section": {"name": form2.text2.data, "icon": "none123",
                                    "banner": "sectionbanner_" + uid + '.jpg', "project_id": program,
                                    "desc": form2.desc.data}}

                c = requests.post(host + "/sections/" + str(sectionid) + "/update", json=data, verify=True)
                return redirect(url_for('.articles', exhibit='program', program=program))
            else:
                flash(u'error')
                return redirect(url_for('.articles', exhibit='addart_section', program=program))

    # ---------------add content----------------#
    form8 = addart_content()
    if request.method == 'POST' and not request.form.get('id'):
        uid = str(uuid.uuid1())
        if request.form.getlist('tagtag'):

            redis_tags = [i for i in tagresp if str(i['id']) in request.form.getlist('tagtag')]
        else:
            redis_tags = None
        if request.form.get('section_id') and request.form.get('section_id') != '-1':
            redis_sectionid = request.form.get('section_id').split('.')[-1]
        else:
            redis_sectionid = None
        if request.form.get('project_id') and request.form.get('project_id') != '-1':
            redis_projectid = request.form.get('project_id').split('.')[-1]
        else:
            redis_projectid = None

        if exhibit == 'addart_content':
            r.hmset('wode', {uid: json.dumps(
                    {'name': request.form.get('content1'), 'img': None, 'contenthtml': request.form.get('editorValue'),
                     'tag_id': redis_tags, 'brief': request.form.get('contentbrief'), \
                     'section_id': redis_sectionid, 'project_id': redis_projectid})})
            r.hdel('wode', request.args.get('save'))

        if request.form.get('content1') and request.form.get('editorValue') and request.form.getlist(
                'tagtag') and request.form.get('contentbrief'):

            file111 = open('app/static/upload/contenthtml_' + uid + '.html', 'wb')
            contenthtml = request.form.get('editorValue')
            # if edit and edit[2]=='content':
            #     pass
            # else:
            #     contenthtml +='''<script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
            #                                             <script>
            #                                                 $(document).ready(function(){
            #                                                     vids = $("[id^='plv_']");
            #
            #                                                     for(var i=0;i<vids.length;i++){
            #                                                         var vid = vids[i].id;
            #
            #                                                         $.ajax({
            #
            #                                                             url: 'https://api.kquestions.com/video/sign',
            #                                                             type: 'GET',
            #                                                             data: {"vid":vid},
            #                                                             success: function(data){
            #
            #                                                                 jQuery.ajax({
            #                                                                       url: "https://player.polyv.net/script/polyvplayer.min.js",
            #                                                                       dataType: "script",
            #                                                                       cache: true
            #                                                                 }).done(function() {
            #                                                                   var player = polyvObject('#'+'plv_'+data['vid']).videoPlayer({
            #                                                                         'width': '900',
            #                                                                         'height': '500',
            #                                                                         'vid': data['vid'],
            #                                                                         'ts': data['ts'],
            #                                                                         'sign': data['sign']
            #                                                                     });
            #                                                                 });
            #
            #                                                             },
            #                                                             error: function(e){
            #                                                                 alert('error')
            #                                                             }
            #
            #                                                         });
            #
            #                                                     }
            #
            #
            #                                                 })
            #                                             </script>
            #                                             '''

            from bs4 import BeautifulSoup
            soupcontent = BeautifulSoup(contenthtml, 'lxml')
            fakevideos = soupcontent.find_all('a', target='_self')

            vids = []

            for fakevideo in fakevideos:
                # vid={'vid':fakevideo['src'],'width':fakevideo['width'],'height':fakevideo['height']}
                # fakevideo.insert_after(BeautifulSoup('''<script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
                #                                         <script>
                #                                             $(document).ready(function(){
                #                                                 vids = $("[id^='plv_']");
                #
                #                                                 for(var i=0;i<vids.length;i++){
                #                                                     var vid = vids[i].id;
                #
                #                                                     $.ajax({
                #                                                         async:false,
                #                                                         url: 'https://admin.kquestions.com/baoli',
                #                                                         type: 'POST',
                #                                                         data: vid,
                #                                                         success: function(data){
                #
                #                                                             jQuery.ajax({
                #                                                                   url: "https://player.polyv.net/script/polyvplayer.min.js",
                #                                                                   dataType: "script",
                #                                                                   cache: true
                #                                                             }).done(function() {
                #                                                               var player = polyvObject('#'+'plv_'+data['vid']).videoPlayer({
                #                                                                     'width': '%s',
                #                                                                     'height': '%s',
                #                                                                     'vid': data['vid'],
                #                                                                     'ts': data['ts'],
                #                                                                     'sign': data['sign']
                #                                                                 });
                #                                                             });
                #
                #                                                         },
                #                                                         error: function(e){
                #                                                             alert('error')
                #                                                         }
                #
                #                                                     });
                #
                #                                                 }
                #
                #
                #                                             })
                #                                         </script>
                #
                #                                         <div id='plv_%s'></div>
                #                 '''%(vid['width'],vid['height'],vid['vid']),'lxml'))
                vid = {'vid': fakevideo['href'][7:], 'width': '420', 'height': '280'}
                fakevideo.insert_after(BeautifulSoup('''
                                                    <center>
                                                        <div id='plv_%s'></div>
                                                    </center>
                                ''' % vid['vid'], 'lxml'))
                fakevideo.extract()
            contenthtmledit = str(soupcontent)

            html_header = open('app/static/html_header.html', 'r')
            file111.write(html_header.read())
            html_header.close()
            file111.write('''<div class="rich_media_content">
  	<div><div class='container' style='width:95%;margin:0 auto; '>''')
            file111.write(
                '''<h2 style='border-bottom:1px solid #e7e7eb;font-size:22px;margin-bottom:14px;padding-bottom:5px;margin-top:5px'>%s</h2>''' % request.form.get(
                    'content1').encode('utf-8'))




            now = datetime.now()

            create_time = now.strftime('%Y-%m-%d')

            if request.files.get('123'):

                request.files.get('123').save('app/static/banners/contentbanner_' + uid + '.jpg')
                contentbanner = Image.open('app/static/banners/contentbanner_' + uid + '.jpg')
                if (contentbanner.width == 1920 and contentbanner.height == 1080) or (
                        contentbanner.width == 1280 and contentbanner.height == 720):

                    if edit and edit[2] == 'content':
                        oldbanner = htmltoname(requests.get(host + '/contents/' + str(content)).json()['banner'])
                        oldhtml = htmltoname(requests.get(host + '/contents/' + str(content)).json()['url'])
                        mydelete.mydelete(oldbanner)
                        mydelete.mydelete(oldhtml)
                        data = {"content": {"name": request.form.get('content1'), "ctype": 1,
                                            'brief': request.form.get('contentbrief'),
                                            "text": contenthtml,
                                            "url": "uploading_" + uid + '.html',
                                            "section_id": request.form.get('section_id').split('.')[-1],
                                            "banner": "uploading_" + uid + '.jpg',  # editresp['banner'],
                                            "tag_ids": request.form.getlist('tagtag')}}  # todo:url and ctype?
                        b = requests.post(host + "/contents/" + str(content) + "/update", json=data, verify=True)
                        file111.write(
                            '''<p style='font-size:16px;color:grey;margin-top:0px'>%s 仟问学堂 <span style='float:right;' id='views_%s'></span></p><br>''' % (
                            create_time, str(content)))

                        file111.write(contenthtmledit)
                        file111.write('''</div></div>
                                        </div>''')
                        file111.close()
                        myupload.upload('contenthtml_' + uid + '.html',
                                        'app/static/upload/contenthtml_' + uid + '.html')
                        myupload.upload('contentbanner_' + uid + '.jpg',
                                        'app/static/banners/contentbanner_' + uid + '.jpg')
                        data['content']['banner'] = "contentbanner_" + uid + '.jpg'
                        data['content']['url'] = "contenthtml_" + uid + '.html'
                        requests.post(host + "/contents/" + str(content) + "/update", json=data, verify=True)
                        print 111
                    else:
                        # print request.form.get('section_id').split('.')[-1]
                        data = {"content": {"name": request.form.get('content1'), "ctype": 1,
                                            "text": contenthtml,
                                            'brief': request.form.get('contentbrief'),
                                            "url": "uploading_" + uid + '.html',
                                            "section_id": request.form.get('section_id').split('.')[-1],
                                            "banner": "uploading_" + uid + '.jpg',  # editresp['banner'],
                                            "tag_ids": request.form.getlist('tagtag')}}  # todo:url and ctype?
                        b = requests.post(host + "/contents/create", json=data, verify=True)
                        # print b.json()
                        contentid = b.json()['id']
                        file111.write(
                            '''<p style='font-size:16px;color:grey;margin-top:0px'>%s 仟问学堂 <span style='float:right;' id='views_%s'></span></p><br>''' % (
                            create_time, contentid))

                        file111.write(contenthtmledit)
                        file111.write('''</div></div>
                                        </div>''')
                        file111.close()
                        myupload.upload('contenthtml_' + uid + '.html',
                                        'app/static/upload/contenthtml_' + uid + '.html')
                        myupload.upload('contentbanner_' + uid + '.jpg',
                                        'app/static/banners/contentbanner_' + uid + '.jpg')
                        data['content']['banner'] = "contentbanner_" + uid + '.jpg'
                        data['content']['url'] = "contenthtml_" + uid + '.html'
                        c = requests.post(host + "/contents/" + str(contentid) + "/update", json=data, verify=True)
                        # print 'licong:', c.json()
                        r.hdel('wode', uid)

                    return redirect(url_for('.articles', exhibit='program', program=program, section=section))


                else:
                    os.remove('app/static/upload/contenthtml_' + uid + '.html')
                    os.remove('app/static/banners/contentbanner_' + uid + '.jpg')
                    flash(u'修改失败,banner尺寸必须是1920*1080或者1280*720')
                    return redirect(url_for('.articles', exhibit='program', program=program, section=section))
            else:
                if exhibit == 'addart_content':
                    flash(u'banner未添加')
                    return redirect(url_for('.articles', exhibit='program', program=program, section=section))
                else:
                    oldhtml = htmltoname(requests.get(host + '/contents/' + str(content)).json()['url'])
                    mydelete.mydelete(oldhtml)
                    data = {"content": {"name": request.form.get('content1'), "ctype": 1,
                                        "text": contenthtml,
                                        'brief': request.form.get('contentbrief'),
                                        "url": "uploading_" + uid + '.html',
                                        "section_id": request.form.get('section_id').split('.')[-1],

                                        "tag_ids": request.form.getlist('tagtag')}}  # todo:url and ctype?
                    b = requests.post(host + "/contents/" + str(content) + "/update", json=data, verify=True)
                    file111.write(
                        '''<p style='font-size:16px;color:grey;margin-top:0px'>%s 仟问学堂 <span style='float:right;' id='views_%s'></span></p><br>''' % (
                        create_time, str(content)))

                    file111.write(contenthtmledit)
                    file111.write('''</div></div>
                                    </div>''')
                    file111.close()
                    myupload.upload('contenthtml_' + uid + '.html', 'app/static/upload/contenthtml_' + uid + '.html')

                    data['content']['url'] = "contenthtml_" + uid + '.html'
                    requests.post(host + "/contents/" + str(content) + "/update", json=data, verify=True)

                    return redirect(url_for('.articles', exhibit='program', program=program, section=section))
        elif request.form.get('program') and request.form.get('editorValue') and request.form.get(
                'programvideo') and request.form.get('programbrief'):
            uid = str(uuid.uuid1())
            if request.files.get('programbanner'):

                request.files.get('programbanner').save('app/static/banners/programbanner_' + uid + '.jpg')
                programbanner = Image.open('app/static/banners/programbanner_' + uid + '.jpg')
                if (programbanner.width == 1920 and programbanner.height == 1080) or (
                        programbanner.width == 1280 and programbanner.height == 720):
                    if edit and edit[2] == 'program':
                        oldbanner = htmltoname(requests.get(host + '/projects/' + str(edit[1])).json()['banner'])
                        mydelete.mydelete(oldbanner)
                        data = {"project": {"name": request.form.get('program'),
                                            "desc": request.form.get('editorValue'),
                                            "video_id": request.form.get('programvideo'),
                                            "brief": request.form.get('programbrief'),
                                            "banner": "uploading_" + uid + '.jpg'
                                            # if request.files.get('programbanner') else programeditresp['banner'],
                                            }}  # todo:url and ctype?
                        b = requests.post(host + "/projects/" + str(edit[1]) + "/update", json=data, verify=True)
                        myupload.upload('programbanner_' + uid + '.jpg',
                                        'app/static/banners/programbanner_' + uid + '.jpg')
                        data['project']['banner'] = 'programbanner_' + uid + '.jpg'
                        requests.post(host + "/projects/" + str(edit[1]) + "/update", json=data, verify=True)
                        print 111
                    else:

                        data = {"project": {"name": request.form.get('program'),
                                            "desc": request.form.get('editorValue'),
                                            "video_id": request.form.get('programvideo'),
                                            "brief": request.form.get('programbrief'),
                                            "banner": "uploading_" + uid + '.jpg',
                                            }}  # todo:url and ctype?
                        b = requests.post(host + "/projects/create", json=data, verify=True)
                        if 'id' in b.json().keys():
                            programid = b.json()['id']
                            myupload.upload('programbanner_' + uid + '.jpg',
                                            'app/static/banners/programbanner_' + uid + '.jpg')
                            data['project']['banner'] = "programbanner_" + uid + '.jpg'
                            b = requests.post(host + "/projects/" + str(programid) + "/update", json=data, verify=True)
                        else:
                            flash('error')

                    return redirect(url_for('.articles', exhibit='program'))

                else:
                    flash(u'修改失败,banner尺寸必须是1920*1080或者1280*720')
                    return redirect(url_for('.articles', exhibit='program'))
            else:
                if exhibit == 'addart_program':
                    flash(u'banner未添加')
                    return redirect(url_for('.articles', exhibit='program'))
                else:

                    data = {"project": {"name": request.form.get('program'),
                                        "desc": request.form.get('editorValue'),
                                        "video_id": request.form.get('programvideo'),
                                        "brief": request.form.get('programbrief')

                                        }}  # todo:url and ctype?
                    b = requests.post(host + "/projects/" + str(edit[1]) + "/update", json=data, verify=True)

                    return redirect(url_for('.articles', exhibit='program'))



        else:
            if 'program' in exhibit or 'content' in exhibit:
                flash(u'出错啦')

    # ---------------edit class form----------------#
    form5 = editart_program()
    # if form5.submitedit.data and form5.validate_on_submit():
    #
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programbanner_" + uid
    #     if form5.iconedit.data:
    #         iconfile = form5.iconedit.data.save('app/static/icons/' + form5.textedit.data + '.ico')
    #         myupload.upload("programicon_" +uid,'app/static/icons/' + form5.textedit.data + '.ico')
    #     if form5.banneredit.data:
    #         bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.ico')
    #         myupload.upload("programbanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.ico')
    #     data = {"project": {"name": form5.textedit.data, "icon": iconurl if form5.iconedit.data else [i[3] for i in programs if i[1] == program][0],
    #                         "banner": bannerurl if form5.banneredit.data else [i[4] for i in programs if i[1] == program][0]}}
    #
    #
    #
    #     editid = edit[1]
    #     edit_column = requests.post(host+'/projects' + '/' + editid + '/update', json=data, verify=True)
    #     print list(edit_column)
    #
    #     return redirect(url_for('.articles', exhibit='program',program=-1))
    # if program != -1 and edit:
    #     form5.textedit.data = [i[0] for i in programs if i[1] == program][0]
    # ---------------edit section form----------------#
    form6 = editart_section()
    if form6.submit2edit.data and form6.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionicon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "sectionbanner_" + uid + '.jpg'
        if form6.icon2edit.data:
            iconfile = form6.icon2edit.data.save('app/static/icons/' + form6.text2edit.data + '.ico')
            myupload.upload("sectionicon_" + uid, 'app/static/icons/' + form6.text2edit.data + '.ico')
        if form6.banner2edit.data:
            oldbanner = htmltoname(requests.get(host + '/sections/' + str(section)).json()['banner'])
            mydelete.mydelete(oldbanner)
            data = {"section": {"name": form6.text2edit.data, "icon": 'none',
                                "banner": "uploading_" + uid + '.jpg', "project_id": program,
                                "desc": form6.descedit.data}}
            requests.post(host + '/sections/' + str(section) + '/update', json=data, verify=True)

            bannerfile = form6.banner2edit.data.save('app/static/banners/' + form6.text2edit.data + '.jpg')
            myupload.upload("sectionbanner_" + uid + '.jpg', 'app/static/banners/' + form6.text2edit.data + '.jpg')
            data['section']['banner'] = "sectionbanner_" + uid + '.jpg'
            requests.post(host + '/sections/' + str(section) + '/update', json=data, verify=True)
            return redirect(url_for('.articles', exhibit='program', program=program))
        else:

            data = {"section": {"name": form6.text2edit.data, "icon": 'none',
                                "project_id": program,
                                "desc": form6.descedit.data}}

            editid = edit[1]
            edit_section = requests.post(host + '/sections' + '/' + editid + '/update', json=data, verify=True)
            return redirect(url_for('.articles', exhibit='program', program=program))
    if section != -1 and edit:
        form6.text2edit.data = [i[0] for i in sections if i[1] == section][0]
        form6.descedit.data = [i[5] for i in sections if i[1] == section][0]
    # ---------------edit lesson form----------------#
    form9 = editart_content()

    #
    # if form9.submit3edit.data and form9.validate_on_submit():
    #     data = { "content" : {  "name": form9.text3edit.data, "ctype": 1,
    #                             "url": form9.texturledit.data, "section_id": section}}
    #
    #
    #     editid = edit[1]
    #     edit_content = requests.post(host+'/contents' + '/' + editid + '/update', json=data, verify=True)
    #     return redirect(url_for('.articles', exhibit='program', program=program, section=section))
    # if content != -1 and edit:
    #     form9.text3edit.data = [i[0] for i in contents if i[1] == content][0]
    #     form9.texturledit.data = [i[4] for i in contents if i[1] == content][0]
    # ---------------delete course or section or lesson----------------#

    import threading
    if delete:
        if delete[2] == 'program':
            deleteid = delete[1]

            deleteinfo = requests.get(host + '/projects' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            if deletebanner:
                mydelete.mydelete(deletebanner)

            delete_program = requests.post(host + '/projects' + '/' + deleteid + '/delete', verify=True)

            return redirect(url_for('.articles', exhibit='program', program=-1))

        elif delete[2] == 'section':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/sections' + '/' + deleteid, verify=True).json()
            mydelete.mydelete(htmltoname(deleteinfo['banner']))
            delete_section = requests.post(host + '/sections' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.articles', exhibit='program', program=program))
        elif delete[2] == 'content':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/contents' + '/' + deleteid, verify=True).json()

            deletehtml = htmltoname(deleteinfo['url'])
            deletebanner = htmltoname(deleteinfo['banner'])
            if deletehtml:
                mydelete.mydelete(deletehtml)
            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_lesson = requests.post(host + '/contents' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.articles', exhibit='program', program=program, section=section))
    content_save = r.hgetall('wode')
    # content_save[1] = base64.decode(content_save[1])
    rcontent_save = []
    if content_save:
        for i in content_save:
            content_save[i] = json.loads(content_save[i])
            # print content_save[i]['project_id']

    return render_template('articles.html', exhibit=exhibit, programs=programs, a=a, program=program, sections=sections,
                           section=section, contents=contents \
                           , content=content, form=form1, form2=form2, form5=form5, form6=form6,
                           form8=form8, form9=form9, editresp=editresp, programeditresp=programeditresp, videos=videos,
                           tagresp=tagresp, content_save=content_save, \
                           belongproject=belongproject, belongsection=belongsection)


@main.route('/resources/<exhibit>', methods=['GET', 'POST'])
def resources(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    courseindex = request.form.get('courseindex')
    chapterindex = request.form.get('chapterindex')
    columnindex = request.form.get('columnindex')
    exerciseindex = request.form.get('exerciseindex')
    indexid = request.form.get('id')
    filtering_word = request.form.get('filtering_word')

    delete = []
    lessondata = request.get_json()
    # if lessondata:
    #     print lessondata


    delete = request.args.getlist('delete')
    column = request.args.get('column', -1, type=int)
    chapter = request.args.get('chapter', -1, type=int)
    course = request.args.get('course', -1, type=int)
    page = request.args.get('page', 1, type=int)
    edit = request.args.getlist('edit')
    if indexid and courseindex:
        a = requests.post(host + '/lessons/' + str(indexid) + '/update',
                          json={'index': courseindex, "chapter_id": chapter})
    if indexid and chapterindex:
        a = requests.post(host + '/chapters/' + str(indexid) + '/update', json={'index': chapterindex})
    if indexid and columnindex:
        a = requests.post(host + '/courses/' + str(indexid) + '/update', json={'index': columnindex})
    if indexid and exerciseindex:
        a = requests.post(host + '/exercises/' + str(indexid) + '/update', json={'index': exerciseindex})
    temptime = []
    temptime.append(time.time())
    videoresp = requests.get(host + '/videos?per=0', verify=True)
    temptime.append(time.time())
    prices = requests.get(host + '/prices', verify=True).json()
    alllessons = requests.get(host + '/lessons?per=0&aud=1', verify=True).json()['records']
    exercises = requests.get(host + '/exercises?per=0', verify=True).json()['records']
    videos = [i for i in videoresp.json()['records']]
    belongcourse = requests.get(host + '/courses?per=0&aud=1', verify=True).json()['records']
    temptime.append(time.time())
    belongchapter = requests.get(host + '/chapters?per=0&aud=1', verify=True).json()['records']
    temptime.append(time.time())
    tagresp = requests.get(host + '/tags?per=0', verify=True).json()['records']
    temptime.append(time.time())
    # ---------------add timu----------------#
    if request.form.get('timu'):
        data = {"exercise": {"answer": request.form.get('answer'), "lesson_id": request.form.get('kecheng'),
                             "body": request.form.get('timu'),
                             "option_a": request.form.get('optiona'), "option_b": request.form.get('optionb'),
                             "option_c": request.form.get('optionc'), "option_d": request.form.get('optiond')}}
        if exhibit == 'addexercise':
            timusend = requests.post(host + '/exercises/create', json=data)
        elif exhibit == 'editexercise':
            timusend = requests.post(host + '/exercises/' + str(request.args.get('editid')) + '/update', json=data)
        return redirect(url_for('.resources', exhibit='exercises'))
    if request.args.get('deleteid'):
        requests.post(host + '/exercises/' + str(request.args.get('deleteid')) + '/delete')
        return redirect(url_for('.resources', exhibit='exercises'))

    # ---------------click button to the new page where you can add course or chapter----------------#
    # form3 = addbutton()
    # if form3.button.data:
    #     return redirect(url_for('.courses', exhibit='addclass', column=column))
    # form4 = addbutton2()
    # if form4.button2.data and form4.validate_on_submit():
    #     return redirect(url_for('.courses', exhibit='addchapter', column=column))
    # form7 = addbutton3()
    # if form7.button3.data and form7.validate_on_submit():
    #     return redirect(url_for('.courses', exhibit='addlesson', column=column, chapter=chapter))
    # ---------------get all courses----------------#
    a = requests.get(host + '/courses?aud=1', verify=True)
    courseresp = requests.get(host + '/courses?per=0&aud=1', verify=True)
    chapters = None
    columns = [[i['name'], i['id'], 'column', i['icon'], i['banner'], i['brief'], i['index'], i['price']] for i in
               a.json()['records']]

    # ---------------get all chapters----------------#
    if column != -1:
        id = str(column)
        d = requests.get(host + '/courses/' + id + '/chapters?aud=1', verify=True)
        chapters = [[i['name'], i['id'], 'chapter', i['icon'], i['banner'], i['desc'], i['index'], i['price']] for i in
                    d.json()['records']]

    # ---------------get all lessons----------------#
    if chapter != -1:
        id = str(chapter)
        lessonresp = requests.get(host + '/lessons?per=0&aud=1', verify=True)

        courses = [
            [i['name'], i['id'], 'lesson', i['desc'], i['browses'], i['created_at'], i['banner'], i['chapter_id'],
             i['course_id'], i['index'], i['price']] for i in lessonresp.json()['records'] if
            i['chapter_id'] == chapter]

    else:
        lessonresp = None
        courses = None
    # ---------------show all lessons----------------#
    if exhibit == 'course':
        id = '1'
        lessonresp = requests.get(host + '/lessons?aud=1&page=' + str(page), verify=True)

        courses = [
            [i['name'], i['id'], 'lesson', i['desc'], i['browses'], i['created_at'], i['banner'], i['chapter_id'],
             i['course_id'], i['index'], i['price']] for i in lessonresp.json()['records']]
        if filtering_word:
            courses = [i for i in courses if filtering_word in i[0]]

    if edit and edit[2] == 'lesson':
        editresp = requests.get(host + '/lessons/' + str(edit[1]), verify=True).json()

    else:
        editresp = None
    if edit and edit[2] == 'chapter':
        chaptereditresp = requests.get(host + '/chapters/' + str(edit[1]), verify=True).json()

    else:
        chaptereditresp = None
    if edit and edit[2] == 'column':
        courseeditresp = requests.get(host + '/courses/' + str(edit[1]), verify=True).json()
        # print courseeditresp

    else:
        courseeditresp = None
    # print editresp

    # ---------------add class----------------#


    form1 = addclass()
    # if form1.submit.data and form1.validate_on_submit():
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "courseicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "coursebanner_" + uid
    #     if form1.banner.data:
    #         bannerfile = form1.banner.data.save('app/static/banners/' + form1.text.data + '.jpg')
    #         myupload.upload("coursebanner_" + uid, 'app/static/banners/' + form1.text.data + '.jpg')
    #     data = {'course':
    #                 {'name': form1.text.data,
    #                  'icon': "none",
    #                  'banner': bannerurl if form1.banner.data else "none",
    #                  'desc': form1.classdesc.data
    #                  }
    #             }
    #
    #
    #
    #     b = requests.post(host+"/courses/create", json=data, verify=True)
    #     return redirect(url_for('.courses', exhibit='column',column=-1))

    # ---------------add chapter----------------#

    form2 = addchapter()
    if request.form.get('chaptername') and request.form.get('chapterdesc') and request.form.get('chapterprice'):
        uid = str(uuid.uuid1())
        if request.files.get('chapterbanner'):
            data = {"chapter": {"name": request.form.get('chaptername'),
                                "banner": "uploading_" + uid + '.jpg', "course_id": column,"audience":1,
                                "desc": request.form.get('chapterdesc'), "price_id": request.form.get('chapterprice')
                                }
                    }
            if exhibit == 'addchapter':
                c = requests.post(host + "/chapters/create", json=data, verify=True)
                chapterid = c.json()['id']
                bannerfile = request.files.get('chapterbanner').save(
                    'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                myupload.upload("chapterbanner_" + uid + '.jpg',
                                'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                data['chapter']['banner'] = "chapterbanner_" + uid + '.jpg'

                c = requests.post(host + "/chapters/" + str(chapterid) + "/update", json=data, verify=True)
            elif exhibit == 'editchapter':
                bannerfile = request.files.get('chapterbanner').save(
                    'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                myupload.upload("chapterbanner_" + uid + '.jpg',
                                'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                data['chapter']['banner'] = "chapterbanner_" + uid + '.jpg'

                c = requests.post(host + "/chapters/" + str(chapter) + "/update", json=data, verify=True)
            return redirect(url_for('.resources', exhibit='column', column=column))
        else:
            if exhibit == 'addchapter':
                flash(u'章节图片未添加')
                return redirect(url_for('.resources', exhibit='addchapter', column=column))
            elif exhibit == 'editchapter':
                data = {"chapter": {"name": request.form.get('chaptername'),
                                    "course_id": column,
                                    "desc": request.form.get('chapterdesc'),
                                    "price_id": request.form.get('chapterprice')
                                    }
                        }
                c = requests.post(host + "/chapters/" + str(chapter) + "/update", json=data, verify=True)
                return redirect(url_for('.resources', exhibit='column', column=column))

    # ---------------add and edit lesson course----------------#
    form8 = addlesson()
    if request.method == 'POST':
        if lessondata and lessondata.has_key('lesson'):
            data = lessondata
            data['lesson']['audience'] = 1
            # print data
            if data['status'] != 'add':
                if lessondata['lesson'].has_key('banner'):
                    aa = base64.b64decode(str(str(lessondata['lesson']['banner']).split(',')[1]))

                    a = open('app/static/banners/123.jpg', 'wb')
                    a.write(aa)
                    a.close()
                    bb = Image.open('app/static/banners/123.jpg')

                    uid = str(uuid.uuid1())

                    if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                        oldbanner = htmltoname(requests.get(host + "/lessons/" + str(data['status'])).json()['banner'])
                        mydelete.mydelete(oldbanner)
                        data['lesson']['banner'] = "uploading_" + uid + '.jpg'
                        myupload.upload('lessonpreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                        data['lesson']['banner'] = "lessonpreview_" + uid + '.jpg'
                        # print data
                        b = requests.post(host + "/lessons/" + str(data['status']) + "/update",
                                          json={'lesson': data[u'lesson']}, verify=True)
                    else:

                        return jsonify({"ok": False})
                else:
                    b = requests.post(host + "/lessons/" + str(data['status']) + "/update",
                                      json={'lesson': data[u'lesson']}, verify=True)

            # print form8.lessonbanner.data,11#.save('/Users/xzsmstephen/Desktop/KQuestions/KQbackstage/app/static/banners/123.jpg')
            # todo:video_id is not all 1

            # lessonbanner=Image.open('/Users/xzsmstephen/Desktop/KQuestions/KQbackstage/app/static/banners/123.jpg')
            # print lessonbanner.width
            elif data['status'] == 'add':
                aa = base64.b64decode(str(str(lessondata['lesson']['banner']).split(',')[1]))

                a = open('app/static/banners/123.jpg', 'wb')
                a.write(aa)
                a.close()
                bb = Image.open('app/static/banners/123.jpg')

                uid = str(uuid.uuid1())

                if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                    data['lesson']['banner'] = "uploading_" + uid + '.jpg'

                    b = requests.post(host + "/lessons/create", json={'lesson': data['lesson']}, verify=True)
                    lessonid = b.json()['id']
                    myupload.upload('lessonpreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                    data['lesson']['banner'] = "lessonpreview_" + uid + '.jpg'
                    # print data
                    b = requests.post(host + "/lessons/" + str(lessonid) + "/update", json={'lesson': data['lesson']},
                                      verify=True)
                else:

                    return jsonify({"ok": False})

            if "id" in b.json().keys():
                return jsonify({"ok": True})
            else:
                print 'error原因:', b.json()
                return jsonify({"ok": False})
        elif lessondata and lessondata.has_key('course'):
            data = lessondata
            data['course']['audience'] = 1
            if data['status'] != 'add':
                if lessondata['course'].has_key('banner'):
                    aa = base64.b64decode(str(str(lessondata['course']['banner']).split(',')[1]))

                    a = open('app/static/banners/123.jpg', 'wb')
                    a.write(aa)
                    a.close()
                    bb = Image.open('app/static/banners/123.jpg')

                    uid = str(uuid.uuid1())

                    if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                        oldbanner = htmltoname(requests.get(host + "/courses/" + str(data['status'])).json()['banner'])
                        mydelete.mydelete(oldbanner)
                        data['course']['banner'] = "uploading_" + uid + '.jpg'

                        requests.post(host + "/courses/" + str(data['status']) + "/update",
                                      json={'course': data[u'course']}, verify=True)
                        myupload.upload('coursepreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                        data['course']['banner'] = "coursepreview_" + uid + '.jpg'
                        # print data
                        b = requests.post(host + "/courses/" + str(data['status']) + "/update",
                                          json={'course': data[u'course']}, verify=True)
                    else:

                        return jsonify({"ok": False})
                else:
                    b = requests.post(host + "/courses/" + str(data['status']) + "/update",
                                      json={'course': data[u'course']}, verify=True)

            elif data['status'] == 'add':
                aa = base64.b64decode(str(str(lessondata['course']['banner']).split(',')[1]))

                a = open('app/static/banners/123.jpg', 'wb')
                a.write(aa)
                a.close()
                bb = Image.open('app/static/banners/123.jpg')

                uid = str(uuid.uuid1())

                if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                    data['course']['banner'] = "uploading_" + uid + '.jpg'
                    b = requests.post(host + "/courses/create", json={'course': data['course']}, verify=True)
                    courseid = b.json()['id']
                    myupload.upload('coursepreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                    data['course']['banner'] = "coursepreview_" + uid + '.jpg'
                    # print data
                    b = requests.post(host + "/courses/" + str(courseid) + "/update", json={'course': data['course']},
                                      verify=True)
                else:

                    return jsonify({"ok": False})

            if "id" in b.json().keys():
                return jsonify({"ok": True})
            else:
                return jsonify({"ok": False})

                # return redirect(url_for('.courses', exhibit='column',column=column,chapter=chapter))

    # ---------------edit class form----------------#
    form5 = editclass()
    # if form5.submitedit.data and form5.validate_on_submit():
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "courseicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "coursebanner_" + uid
    #     if form5.banneredit.data:
    #         bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.jpg')
    #         myupload.upload("coursebanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.jpg')
    #     data = {"course": {"name": form5.textedit.data,
    #                         "icon": "none",
    #
    #                         "banner": bannerurl if form5.banneredit.data else
    #                         [i[4] for i in columns if i[1] == column][0],"desc": form5.classdescedit.data}}
    #
    #
    #     editid = edit[1]
    #     edit_column = requests.post(host+'/courses' + '/' + editid + '/update',json=data, verify=True)
    #
    #
    #     return redirect(url_for('.courses', exhibit='column',column=-1))
    # if column!=-1 and edit:
    #     form5.textedit.data = [i[0] for i in columns if i[1]==column][0]
    #     form5.classdescedit.data = [i[5] for i in columns if i[1]==column][0]
    # ---------------edit chapter form----------------#
    form6 = editchapter()
    if form6.submit2edit.data and form6.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chaptericon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chapterbanner_" + uid + '.jpg'
        editid = edit[1]
        if form6.icon2edit.data:
            iconfile = form6.icon2edit.data.save('app/static/icons/' + form6.text2edit.data + '.ico')
            myupload.upload("chaptericon_" + uid, 'app/static/icons/' + form6.text2edit.data + '.ico')
        if form6.banner2edit.data:
            oldbannerurl = htmltoname([i[4] for i in chapters if i[1] == chapter][0])
            # print oldbannerurl
            mydelete.mydelete(oldbannerurl)
            data = {"chapter": {"name": form6.text2edit.data,
                                "icon": 'none',
                                "banner": "uploading_" + uid + '.jpg',
                                "desc": form6.descedit.data,
                                "course_id": column}}

            requests.post(host + '/chapters' + '/' + editid + '/update', json=data, verify=True)
            bannerfile = form6.banner2edit.data.save('app/static/banners/' + form6.text2edit.data + '.jpg')
            myupload.upload("chapterbanner_" + uid + '.jpg', 'app/static/banners/' + form6.text2edit.data + '.jpg')
            data['chapter']['banner'] = "chapterbanner_" + uid + '.jpg'
            edit_chapter = requests.post(host + '/chapters' + '/' + editid + '/update', json=data, verify=True)
            # print edit_chapter.json()
        else:
            data = {"chapter": {"name": form6.text2edit.data,

                                "desc": form6.descedit.data,
                                "course_id": column}}
            edit_chapter = requests.post(host + '/chapters' + '/' + editid + '/update', json=data, verify=True)
            # print edit_chapter.json()

        return redirect(url_for('.resources', exhibit='column', column=column))
    if chapter != -1 and edit:
        form6.text2edit.data = [i[0] for i in chapters if i[1] == chapter][0]
        form6.descedit.data = [i[5] for i in chapters if i[1] == chapter][0]
    # ---------------edit lesson form----------------#
    form9 = editlesson()

    # if form9.submit3edit.data and form9.validate_on_submit():
    #     data = {"lesson": {"name": form9.text3edit.data, "banner": "bbbbanner",
    #                        "browses": [i[4] for i in courses if i[1] == course][0], "desc": form9.lessondescedit.data,\
    #                        "chapter_id": form9.belongchapteredit.data,"course_id":column,"video_id":1}}2
    #     #todo:browse will be send?and lessons have banner and video_id is not 1
    #
    #     editid = edit[1]
    #     edit_chapter = requests.post(host+'/lessons' + '/' + editid + '/update', json=data, verify=True)
    #     print list(edit_chapter)
    #
    #     # elif edit[2] == 'chapter':
    #     #     editid = edit[1]
    #     #     edit_chapter = requests.post('http://192.168.1.48:3000' + '/chapters' + '/' + editid + '/update')
    #
    #
    #
    #     return redirect(url_for('.courses', exhibit='column', column=column,chapter=chapter))
    # if course != -1 and edit:
    #
    #     form9.text3edit.data = [i[0] for i in courses if i[1] == course][0]
    #     form9.lessondescedit.data = [i[3] for i in courses if i[1] == course][0]
    #     form9.belongchapteredit.data =str([i[7] for i in courses if i[1] == course][0])
    # ---------------delete course or chapter or lesson----------------#
    if delete:
        if delete[2] == 'column':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/courses' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            # print deletebanner

            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_column = requests.post(host + '/courses' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.resources', exhibit='column', column=-1))

        elif delete[2] == 'chapter':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/chapters' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            # print deletebanner

            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_chapter = requests.post(host + '/chapters' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.resources', exhibit='column', column=column))
        elif delete[2] == 'lesson':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/lessons' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            # print deletebanner

            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_chapter = requests.post(host + '/chapters' + '/' + deleteid + '/delete', verify=True)
            delete_lesson = requests.post(host + '/lessons' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.resources', exhibit='column', column=column, chapter=chapter))
    temptime.append(time.time())
    # print temptime




    return render_template('resources.html', exhibit=exhibit, columns=columns, a=a, column=column, chapters=chapters,
                           chapter=chapter, courses=courses \
                           , course=course, form=form1, form2=form2, form5=form5, form6=form6,
                           form8=form8, form9=form9, videos=videos, edit=edit,
                           lessonresp=lessonresp, editresp=editresp, courseresp=courseresp,
                           courseeditresp=courseeditresp, page=page,
                           belongcourse=belongcourse, belongchapter=belongchapter, tagresp=tagresp, exercises=exercises,
                           alllessons=alllessons, prices=prices, chaptereditresp=chaptereditresp)
@main.route('/courses/<exhibit>', methods=['GET', 'POST'])
def courses(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    courseindex = request.form.get('courseindex')
    chapterindex = request.form.get('chapterindex')
    columnindex = request.form.get('columnindex')
    exerciseindex = request.form.get('exerciseindex')
    indexid = request.form.get('id')
    filtering_word = request.form.get('filtering_word')

    delete = []
    lessondata = request.get_json()
    # if lessondata:
    #     print lessondata


    delete = request.args.getlist('delete')
    column = request.args.get('column', -1, type=int)
    chapter = request.args.get('chapter', -1, type=int)
    course = request.args.get('course', -1, type=int)
    page = request.args.get('page', 1, type=int)
    edit = request.args.getlist('edit')
    if indexid and courseindex:
        a = requests.post(host + '/lessons/' + str(indexid) + '/update',
                          json={'index': courseindex, "chapter_id": chapter})
    if indexid and chapterindex:
        a = requests.post(host + '/chapters/' + str(indexid) + '/update', json={'index': chapterindex})
    if indexid and columnindex:
        a = requests.post(host + '/courses/' + str(indexid) + '/update', json={'index': columnindex})
    if indexid and exerciseindex:
        a = requests.post(host + '/exercises/' + str(indexid) + '/update', json={'index': exerciseindex})
    temptime = []
    temptime.append(time.time())
    videoresp = requests.get(host + '/videos?per=0', verify=True)
    temptime.append(time.time())
    prices = requests.get(host + '/prices', verify=True).json()
    # print requests.get(host + '/lessons?per=0', verify=True).json()
    alllessons = requests.get(host + '/lessons?per=0', verify=True).json()['records']
    exercises = requests.get(host + '/exercises?per=0', verify=True).json()['records']
    videos = [i for i in videoresp.json()['records']]
    belongcourse = requests.get(host + '/courses?per=0', verify=True).json()['records']
    temptime.append(time.time())
    belongchapter = requests.get(host + '/chapters?per=0', verify=True).json()['records']
    temptime.append(time.time())
    tagresp = requests.get(host + '/tags?per=0', verify=True).json()['records']
    temptime.append(time.time())
    # ---------------add timu----------------#
    if request.form.get('timu'):
        data = {"exercise": {"answer": request.form.get('answer'), "lesson_id": request.form.get('kecheng'),
                             "body": request.form.get('timu'),
                             "option_a": request.form.get('optiona'), "option_b": request.form.get('optionb'),
                             "option_c": request.form.get('optionc'), "option_d": request.form.get('optiond')}}
        if exhibit == 'addexercise':
            timusend = requests.post(host + '/exercises/create', json=data)
        elif exhibit == 'editexercise':
            timusend = requests.post(host + '/exercises/' + str(request.args.get('editid')) + '/update', json=data)
        return redirect(url_for('.courses', exhibit='exercises'))
    if request.args.get('deleteid'):
        requests.post(host + '/exercises/' + str(request.args.get('deleteid')) + '/delete')
        return redirect(url_for('.courses', exhibit='exercises'))

    # ---------------click button to the new page where you can add course or chapter----------------#
    form3 = addbutton()
    if form3.button.data:
        return redirect(url_for('.courses', exhibit='addclass', column=column))
    form4 = addbutton2()
    if form4.button2.data and form4.validate_on_submit():
        return redirect(url_for('.courses', exhibit='addchapter', column=column))
    form7 = addbutton3()
    if form7.button3.data and form7.validate_on_submit():
        return redirect(url_for('.courses', exhibit='addlesson', column=column, chapter=chapter))
    # ---------------get all courses----------------#
    a = requests.get(host + '/courses', verify=True)
    courseresp = requests.get(host + '/courses?per=0', verify=True)
    chapters = None
    columns = [[i['name'], i['id'], 'column', i['icon'], i['banner'], i['brief'], i['index'], i['price']] for i in
               a.json()['records']]

    # ---------------get all chapters----------------#
    if column != -1:
        id = str(column)
        d = requests.get(host + '/courses/' + id + '/chapters', verify=True)
        chapters = [[i['name'], i['id'], 'chapter', i['icon'], i['banner'], i['desc'], i['index'], i['price']] for i in
                    d.json()['records']]

    # ---------------get all lessons----------------#
    if chapter != -1:
        id = str(chapter)
        lessonresp = requests.get(host + '/lessons?per=0', verify=True)

        courses = [
            [i['name'], i['id'], 'lesson', i['desc'], i['browses'], i['created_at'], i['banner'], i['chapter_id'],
             i['course_id'], i['index'], i['price']] for i in lessonresp.json()['records'] if
            i['chapter_id'] == chapter]

    else:
        lessonresp = None
        courses = None
    # ---------------show all lessons----------------#
    if exhibit == 'course':
        id = '1'
        lessonresp = requests.get(host + '/lessons?page=' + str(page), verify=True)

        courses = [
            [i['name'], i['id'], 'lesson', i['desc'], i['browses'], i['created_at'], i['banner'], i['chapter_id'],
             i['course_id'], i['index'], i['price']] for i in lessonresp.json()['records']]
        if filtering_word:
            courses = [i for i in courses if filtering_word in i[0]]

    if edit and edit[2] == 'lesson':
        editresp = requests.get(host + '/lessons/' + str(edit[1]), verify=True).json()

    else:
        editresp = None
    if edit and edit[2] == 'chapter':
        chaptereditresp = requests.get(host + '/chapters/' + str(edit[1]), verify=True).json()

    else:
        chaptereditresp = None
    if edit and edit[2] == 'column':
        courseeditresp = requests.get(host + '/courses/' + str(edit[1]), verify=True).json()
        # print courseeditresp

    else:
        courseeditresp = None
    # print editresp

    # ---------------add class----------------#


    form1 = addclass()
    # if form1.submit.data and form1.validate_on_submit():
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "courseicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "coursebanner_" + uid
    #     if form1.banner.data:
    #         bannerfile = form1.banner.data.save('app/static/banners/' + form1.text.data + '.jpg')
    #         myupload.upload("coursebanner_" + uid, 'app/static/banners/' + form1.text.data + '.jpg')
    #     data = {'course':
    #                 {'name': form1.text.data,
    #                  'icon': "none",
    #                  'banner': bannerurl if form1.banner.data else "none",
    #                  'desc': form1.classdesc.data
    #                  }
    #             }
    #
    #
    #
    #     b = requests.post(host+"/courses/create", json=data, verify=True)
    #     return redirect(url_for('.courses', exhibit='column',column=-1))

    # ---------------add chapter----------------#

    form2 = addchapter()
    if request.form.get('chaptername') and request.form.get('chapterdesc') and request.form.get('chapterprice'):
        uid = str(uuid.uuid1())
        if request.files.get('chapterbanner'):
            data = {"chapter": {"name": request.form.get('chaptername'),
                                "banner": "uploading_" + uid + '.jpg', "course_id": column,
                                "desc": request.form.get('chapterdesc'), "price_id": request.form.get('chapterprice')
                                }
                    }
            if exhibit == 'addchapter':
                c = requests.post(host + "/chapters/create", json=data, verify=True)
                chapterid = c.json()['id']
                bannerfile = request.files.get('chapterbanner').save(
                    'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                myupload.upload("chapterbanner_" + uid + '.jpg',
                                'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                data['chapter']['banner'] = "chapterbanner_" + uid + '.jpg'

                c = requests.post(host + "/chapters/" + str(chapterid) + "/update", json=data, verify=True)
            elif exhibit == 'editchapter':
                bannerfile = request.files.get('chapterbanner').save(
                    'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                myupload.upload("chapterbanner_" + uid + '.jpg',
                                'app/static/banners/' + request.form.get('chaptername') + '.jpg')
                data['chapter']['banner'] = "chapterbanner_" + uid + '.jpg'

                c = requests.post(host + "/chapters/" + str(chapter) + "/update", json=data, verify=True)
            return redirect(url_for('.courses', exhibit='column', column=column))
        else:
            if exhibit == 'addchapter':
                flash(u'章节图片未添加')
                return redirect(url_for('.courses', exhibit='addchapter', column=column))
            elif exhibit == 'editchapter':
                data = {"chapter": {"name": request.form.get('chaptername'),
                                    "course_id": column,
                                    "desc": request.form.get('chapterdesc'),
                                    "price_id": request.form.get('chapterprice')
                                    }
                        }
                c = requests.post(host + "/chapters/" + str(chapter) + "/update", json=data, verify=True)
                return redirect(url_for('.courses', exhibit='column', column=column))

    # ---------------add and edit lesson course----------------#
    form8 = addlesson()
    if request.method == 'POST':
        if lessondata and lessondata.has_key('lesson'):
            data = lessondata
            # print data
            if data['status'] != 'add':
                if lessondata['lesson'].has_key('banner'):
                    aa = base64.b64decode(str(str(lessondata['lesson']['banner']).split(',')[1]))

                    a = open('app/static/banners/123.jpg', 'wb')
                    a.write(aa)
                    a.close()
                    bb = Image.open('app/static/banners/123.jpg')

                    uid = str(uuid.uuid1())

                    if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                        oldbanner = htmltoname(requests.get(host + "/lessons/" + str(data['status'])).json()['banner'])
                        mydelete.mydelete(oldbanner)
                        data['lesson']['banner'] = "uploading_" + uid + '.jpg'
                        myupload.upload('lessonpreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                        data['lesson']['banner'] = "lessonpreview_" + uid + '.jpg'
                        # print data
                        b = requests.post(host + "/lessons/" + str(data['status']) + "/update",
                                          json={'lesson': data[u'lesson']}, verify=True)
                    else:

                        return jsonify({"ok": False})
                else:
                    b = requests.post(host + "/lessons/" + str(data['status']) + "/update",
                                      json={'lesson': data[u'lesson']}, verify=True)

            # print form8.lessonbanner.data,11#.save('/Users/xzsmstephen/Desktop/KQuestions/KQbackstage/app/static/banners/123.jpg')
            # todo:video_id is not all 1

            # lessonbanner=Image.open('/Users/xzsmstephen/Desktop/KQuestions/KQbackstage/app/static/banners/123.jpg')
            # print lessonbanner.width
            elif data['status'] == 'add':
                aa = base64.b64decode(str(str(lessondata['lesson']['banner']).split(',')[1]))

                a = open('app/static/banners/123.jpg', 'wb')
                a.write(aa)
                a.close()
                bb = Image.open('app/static/banners/123.jpg')

                uid = str(uuid.uuid1())

                if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                    data['lesson']['banner'] = "uploading_" + uid + '.jpg'
                    b = requests.post(host + "/lessons/create", json={'lesson': data['lesson']}, verify=True)
                    lessonid = b.json()['id']
                    myupload.upload('lessonpreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                    data['lesson']['banner'] = "lessonpreview_" + uid + '.jpg'
                    # print data
                    b = requests.post(host + "/lessons/" + str(lessonid) + "/update", json={'lesson': data['lesson']},
                                      verify=True)
                else:

                    return jsonify({"ok": False})

            if "id" in b.json().keys():
                return jsonify({"ok": True})
            else:
                print 'error原因:', b.json()
                return jsonify({"ok": False})
        elif lessondata and lessondata.has_key('course'):
            data = lessondata
            if data['status'] != 'add':
                if lessondata['course'].has_key('banner'):
                    aa = base64.b64decode(str(str(lessondata['course']['banner']).split(',')[1]))

                    a = open('app/static/banners/123.jpg', 'wb')
                    a.write(aa)
                    a.close()
                    bb = Image.open('app/static/banners/123.jpg')

                    uid = str(uuid.uuid1())

                    if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                        oldbanner = htmltoname(requests.get(host + "/courses/" + str(data['status'])).json()['banner'])
                        mydelete.mydelete(oldbanner)
                        data['course']['banner'] = "uploading_" + uid + '.jpg'
                        requests.post(host + "/courses/" + str(data['status']) + "/update",
                                      json={'course': data[u'course']}, verify=True)
                        myupload.upload('coursepreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                        data['course']['banner'] = "coursepreview_" + uid + '.jpg'
                        # print data
                        b = requests.post(host + "/courses/" + str(data['status']) + "/update",
                                          json={'course': data[u'course']}, verify=True)
                    else:

                        return jsonify({"ok": False})
                else:
                    b = requests.post(host + "/courses/" + str(data['status']) + "/update",
                                      json={'course': data[u'course']}, verify=True)

            elif data['status'] == 'add':
                aa = base64.b64decode(str(str(lessondata['course']['banner']).split(',')[1]))

                a = open('app/static/banners/123.jpg', 'wb')
                a.write(aa)
                a.close()
                bb = Image.open('app/static/banners/123.jpg')

                uid = str(uuid.uuid1())

                if (bb.width == 1920 and bb.height == 1080) or (bb.width == 1280 and bb.height == 720):
                    data['course']['banner'] = "uploading_" + uid + '.jpg'
                    b = requests.post(host + "/courses/create", json={'course': data['course']}, verify=True)
                    courseid = b.json()['id']
                    myupload.upload('coursepreview_' + uid + '.jpg', 'app/static/banners/123.jpg')
                    data['course']['banner'] = "coursepreview_" + uid + '.jpg'
                    # print data
                    b = requests.post(host + "/courses/" + str(courseid) + "/update", json={'course': data['course']},
                                      verify=True)
                else:

                    return jsonify({"ok": False})

            if "id" in b.json().keys():
                return jsonify({"ok": True})
            else:
                return jsonify({"ok": False})

                # return redirect(url_for('.courses', exhibit='column',column=column,chapter=chapter))

    # ---------------edit class form----------------#
    form5 = editclass()
    # if form5.submitedit.data and form5.validate_on_submit():
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "courseicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "coursebanner_" + uid
    #     if form5.banneredit.data:
    #         bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.jpg')
    #         myupload.upload("coursebanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.jpg')
    #     data = {"course": {"name": form5.textedit.data,
    #                         "icon": "none",
    #
    #                         "banner": bannerurl if form5.banneredit.data else
    #                         [i[4] for i in columns if i[1] == column][0],"desc": form5.classdescedit.data}}
    #
    #
    #     editid = edit[1]
    #     edit_column = requests.post(host+'/courses' + '/' + editid + '/update',json=data, verify=True)
    #
    #
    #     return redirect(url_for('.courses', exhibit='column',column=-1))
    # if column!=-1 and edit:
    #     form5.textedit.data = [i[0] for i in columns if i[1]==column][0]
    #     form5.classdescedit.data = [i[5] for i in columns if i[1]==column][0]
    # ---------------edit chapter form----------------#
    form6 = editchapter()
    if form6.submit2edit.data and form6.validate_on_submit():
        uid = str(uuid.uuid1())
        iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chaptericon_" + uid
        bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "chapterbanner_" + uid + '.jpg'
        editid = edit[1]
        if form6.icon2edit.data:
            iconfile = form6.icon2edit.data.save('app/static/icons/' + form6.text2edit.data + '.ico')
            myupload.upload("chaptericon_" + uid, 'app/static/icons/' + form6.text2edit.data + '.ico')
        if form6.banner2edit.data:
            oldbannerurl = htmltoname([i[4] for i in chapters if i[1] == chapter][0])
            # print oldbannerurl
            mydelete.mydelete(oldbannerurl)
            data = {"chapter": {"name": form6.text2edit.data,
                                "icon": 'none',
                                "banner": "uploading_" + uid + '.jpg',
                                "desc": form6.descedit.data,
                                "course_id": column}}

            requests.post(host + '/chapters' + '/' + editid + '/update', json=data, verify=True)
            bannerfile = form6.banner2edit.data.save('app/static/banners/' + form6.text2edit.data + '.jpg')
            myupload.upload("chapterbanner_" + uid + '.jpg', 'app/static/banners/' + form6.text2edit.data + '.jpg')
            data['chapter']['banner'] = "chapterbanner_" + uid + '.jpg'
            edit_chapter = requests.post(host + '/chapters' + '/' + editid + '/update', json=data, verify=True)
            # print edit_chapter.json()
        else:
            data = {"chapter": {"name": form6.text2edit.data,

                                "desc": form6.descedit.data,
                                "course_id": column}}
            edit_chapter = requests.post(host + '/chapters' + '/' + editid + '/update', json=data, verify=True)
            # print edit_chapter.json()

        return redirect(url_for('.courses', exhibit='column', column=column))
    if chapter != -1 and edit:
        form6.text2edit.data = [i[0] for i in chapters if i[1] == chapter][0]
        form6.descedit.data = [i[5] for i in chapters if i[1] == chapter][0]
    # ---------------edit lesson form----------------#
    form9 = editlesson()

    # if form9.submit3edit.data and form9.validate_on_submit():
    #     data = {"lesson": {"name": form9.text3edit.data, "banner": "bbbbanner",
    #                        "browses": [i[4] for i in courses if i[1] == course][0], "desc": form9.lessondescedit.data,\
    #                        "chapter_id": form9.belongchapteredit.data,"course_id":column,"video_id":1}}
    #     #todo:browse will be send?and lessons have banner and video_id is not 1
    #
    #     editid = edit[1]
    #     edit_chapter = requests.post(host+'/lessons' + '/' + editid + '/update', json=data, verify=True)
    #     print list(edit_chapter)
    #
    #     # elif edit[2] == 'chapter':
    #     #     editid = edit[1]
    #     #     edit_chapter = requests.post('http://192.168.1.48:3000' + '/chapters' + '/' + editid + '/update')
    #
    #
    #
    #     return redirect(url_for('.courses', exhibit='column', column=column,chapter=chapter))
    # if course != -1 and edit:
    #
    #     form9.text3edit.data = [i[0] for i in courses if i[1] == course][0]
    #     form9.lessondescedit.data = [i[3] for i in courses if i[1] == course][0]
    #     form9.belongchapteredit.data =str([i[7] for i in courses if i[1] == course][0])
    # ---------------delete course or chapter or lesson----------------#
    if delete:
        if delete[2] == 'column':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/courses' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            # print deletebanner

            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_column = requests.post(host + '/courses' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.courses', exhibit='column', column=-1))

        elif delete[2] == 'chapter':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/chapters' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            # print deletebanner

            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_chapter = requests.post(host + '/chapters' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.courses', exhibit='column', column=column))
        elif delete[2] == 'lesson':
            deleteid = delete[1]
            deleteinfo = requests.get(host + '/lessons' + '/' + deleteid, verify=True).json()

            deletebanner = htmltoname(deleteinfo['banner'])
            # print deletebanner

            if deletebanner:
                mydelete.mydelete(deletebanner)
            delete_chapter = requests.post(host + '/chapters' + '/' + deleteid + '/delete', verify=True)
            delete_lesson = requests.post(host + '/lessons' + '/' + deleteid + '/delete', verify=True)
            return redirect(url_for('.courses', exhibit='column', column=column, chapter=chapter))
    temptime.append(time.time())
    # print temptime




    return render_template('courses.html', exhibit=exhibit, columns=columns, a=a, column=column, chapters=chapters,
                           chapter=chapter, courses=courses \
                           , course=course, form=form1, form2=form2, form3=form3, form4=form4, form5=form5, form6=form6,
                           form7=form7, form8=form8, form9=form9, videos=videos, edit=edit,
                           lessonresp=lessonresp, editresp=editresp, courseresp=courseresp,
                           courseeditresp=courseeditresp, page=page,
                           belongcourse=belongcourse, belongchapter=belongchapter, tagresp=tagresp, exercises=exercises,
                           alllessons=alllessons, prices=prices, chaptereditresp=chaptereditresp)


@main.route('/media/<exhibit>', methods=['POST', 'GET'])
def media(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    deleteid = request.args.get('deleteid', -1, type=int)
    filtering_word = request.form.get('filtering_word')
    editid = request.args.get('editid', -1, type=int)
    media = request.args.get('media', -1, type=int)
    video = request.args.get('video', -1, type=int)
    mysubmit = request.args.get('mysubmit', -1, type=int)
    page = request.args.get('page', 1, type=int)
    videoresp = requests.get(host + '/videos?page=' + str(page), verify=True)
    token = 'Bearer ' + json.loads(r.hget('admin', session['adminid']))['access_token']

    if filtering_word:
        videos = [i for i in videoresp.json()['records'] if filtering_word in i['name']]
    else:
        videos = [i for i in videoresp.json()['records']]
    videoinfo = request.get_json()
    if deleteid != -1:
        deleteid = str(deleteid)
        # print deleteid



        deleteinfo = requests.get(host + '/videos/' + deleteid, verify=True).json()
        secretkey = 'dO3eMfJ1Nq'
        data = {'writetoken': '4eb2be96-fed2-4728-a94d-d8b62ca36670', "vid": deleteinfo['url']}
        sha1 = hashlib.sha1()
        sign = 'vid=' + data['vid'] + '&writetoken=' + data['writetoken'] + secretkey
        # sign = 'writetoken='+data['writetoken'] +'vid='+data['vid']+secretkey
        sha1.update(sign)

        sign = sha1.hexdigest()
        data['sign'] = sign
        deletebanner = requests.post('http://v.polyv.net/uc/services/rest?method=delVideoById', data=data)
        # print deletebanner.json()
        requests.post(host + '/videos/' + deleteid + '/delete', verify=True)

        return redirect(url_for('.media', exhibit='show_media',page=page))

    if videoinfo:
        data1 = videoinfo['data'][0]
        # print videoinfo['data']
        data = {"video": {"name": data1['title'],
                          "length": reduce(lambda x, y: int(x) * 60 + int(y), data1['duration'].split(':')),
                          "width": int(data1['playerwidth']),
                          "height": int(data1['playerheight']),
                          "filesize": int(data1['source_filesize']),
                          "brief": data1['context'],
                          "url": data1['vid'],
                          "preview": data1['first_image']}}
        b = requests.post(host + '/videos/create', json=data, verify=True)
        # print b.json()
        return jsonify({'ok': 1})
    # ---------------add program(project)----------------#

    #
    #     b = requests.post("http://" + host + "/projects/create", json=data)
    #     return redirect(url_for('.articles', exhibit='program', program=-1))



    # ---------------get all programs(projects)----------------#




    # # ---------------edit class form----------------#
    # form5 = editart_program()
    # if form5.submitedit.data and form5.validate_on_submit():
    #
    #     uid = str(uuid.uuid1())
    #     iconurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programicon_" + uid
    #     bannerurl = "http://oh8c4fk40.bkt.clouddn.com/" + "programbanner_" + uid
    #     if form5.iconedit.data:
    #         iconfile = form5.iconedit.data.save('app/static/icons/' + form5.textedit.data + '.ico')
    #         myupload.upload("programicon_" + uid, 'app/static/icons/' + form5.textedit.data + '.ico')
    #     if form5.banneredit.data:
    #         bannerfile = form5.banneredit.data.save('app/static/banners/' + form5.textedit.data + '.ico')
    #         myupload.upload("programbanner_" + uid, 'app/static/banners/' + form5.textedit.data + '.ico')
    #     data = {"project": {"name": form5.textedit.data,
    #                         "icon": iconurl if form5.iconedit.data else [i[3] for i in programs if i[1] == program][0],
    #                         "banner": bannerurl if form5.banneredit.data else
    #                         [i[4] for i in programs if i[1] == program][0]}}
    #
    #     editid = edit[1]
    #     edit_column = requests.post('http://' + host + '/projects' + '/' + editid + '/update', json=data)
    #     print list(edit_column)
    #
    #     return redirect(url_for('.articles', exhibit='program', program=-1))
    # if program != -1 and edit:
    #     form5.textedit.data = [i[0] for i in programs if i[1] == program][0]
    #
    # # ---------------delete course or section or lesson----------------#
    # if delete:
    #     if delete[2] == 'program':
    #         deleteid = delete[1]
    #         delete_program = requests.post('http://' + host + '/projects' + '/' + deleteid + '/delete')
    #         return redirect(url_for('.articles', exhibit='program', program=-1))



    return render_template('media.html', exhibit=exhibit, video=video, videos=videos, media=media, videoresp=videoresp,
                           page=page, token=token)


@main.route('/videoshow/<videoid>', methods=["GET", "POST"])
def videoshow(videoid):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    videoresp = requests.get(host + '/videos/' + videoid).json()['url']
    uploadok = request.args.get('ok')
    editid = videoid
    editid = str(editid)
    editinfo = requests.get(host + '/videos/' + editid, verify=True).json()
    secretkey = 'dO3eMfJ1Nq'
    data = {'writetoken': '4eb2be96-fed2-4728-a94d-d8b62ca36670', "vid": editinfo['url']}
    sha1 = hashlib.sha1()
    sign = 'vid=' + data['vid'] + '&writetoken=' + data['writetoken'] + secretkey
    sha1.update(sign)
    sign = sha1.hexdigest()
    data['sign'] = sign
    if uploadok == 'uploadok':
        readdata = {'readtoken': 'ae449255-843b-4b9e-9884-2e01bfaf0978', "vid": editinfo['url']}
        sha1 = hashlib.sha1()
        sign = 'readtoken=' + readdata['readtoken'] + '&vid=' + readdata['vid'] + secretkey
        sha1.update(sign)
        sign = sha1.hexdigest()
        readdata['sign'] = sign
        # print readdata
        videoinfo = requests.post('http://v.polyv.net/uc/services/rest?method=getById', data=readdata).json()
        requests.post(host + '/videos/' + videoid + '/update', json={'preview': videoinfo['data'][0]['first_image']})

    # data['Filedata']= request.form.get('file')
    # editbanner = requests.post('http://v.polyv.net/uc/services/rest?method=upFirstImage',data=data)
    return render_template('videoshow.html', videoid=videoid, videoresp=videoresp, data=data)


@main.route('/admins/<exhibit>', methods=['GET', 'POST'])
def admin(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    editid = request.args.get('editid', -1, type=int)
    buyid = request.args.get('buyid', -1, type=int)
    buydeleteid=request.args.get('buydeleteid', -1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    upage=request.args.get('upage',1,type=int)
    trypage=request.args.get('trypage',1,type=int)
    trypage2=request.args.get('trypage2',1,type=int)
    truepage=request.args.get('truepage',1,type=int)
    users=None
    try_users=None
    true_users=None
    try_users2=None
    if exhibit=='users':
        users = requests.get(host + '/users?page='+str(upage), verify=True)
    elif exhibit=='member':
        try_users = requests.get(host + '/users?mtype=1&page='+str(trypage), verify=True)
        true_users = requests.get(host + '/users?mtype=2&page='+str(truepage), verify=True)
        try_users2=requests.get(host + '/users?mtype=3&page='+str(trypage2), verify=True)

    members_stu = requests.get(host + '/members?per=0&utype=0', verify=True).json()
    members_tea = requests.get(host + '/members?per=0', verify=True).json()
    members = members_stu +members_tea
    # if not i['avatar'].startswith('uploading_')
    if buyid != -1:
        if request.form.getlist('buycourse'):
            for i in request.form.getlist('buycourse'):
                data = {"course_purchase": {"user_id": buyid, "course_id": i}}
                requests.post(host + '/course_purchases/create', json=data)
        if request.form.getlist('buylesson'):
            for i in request.form.getlist('buylesson'):
                data = {"lesson_purchase": {"user_id": buyid, "lesson_id": i}}
                requests.post(host + '/lesson_purchases/create', json=data)
        if request.form.get('buyvip') :
            data = {"subscription": {
                "user_id": buyid, "member_id": request.form.get('buyvip'),"period_start":request.form.get('begintime')
                }}
            a=requests.post(host + '/subscriptions/create', json=data)
            print a.json()
    if buydeleteid != -1:
        requests.post(host + '/subscriptions/'+str(buydeleteid)+'/delete')
    if exhibit=='users':
        vip_purchases=None
    else:
        vip_purchases = requests.get(host + '/subscriptions?per=0').json()['records']
    #: -----------------adduser-------------------#
    form1 = adduser()

    if form1.submit.data and form1.validate_on_submit():

        # if form1.avatar.data:

                uid = str(uuid.uuid1())
            #
            # avatarfile = form1.avatar.data.save('app/static/icons/' + form1.nick.data + '.png')
            # avat = Image.open('app/static/icons/' + form1.nick.data + '.png')
            # if avat.width == 320 and avat.height == 320:
                data = {"user": {"phone": form1.phone.data, "utype": int(form1.utype.data),
                                  "nick": form1.nick.data,
                                 "gender": form1.gender.data,
                                 "password":"123456"}}
                b = requests.post(host + '/users/create', json=data, verify=True)
                if 'id' in b.json().keys():
                    # print b
                    userid = b.json()['id']
                    # myupload.upload("useravatar_" + uid + '.png', 'app/static/icons/' + form1.nick.data + '.png')
                    data = {"user": {"phone": form1.phone.data, "utype": int(form1.utype.data),
                                     "nick": form1.nick.data,
                                     "gender": form1.gender.data,
                                     "password":"123456"}}
                    b = requests.post(host + '/users/' + str(userid) + '/update', json=data, verify=True)
                    # print b.json()
                    if 'id' in b.json().keys():
                        return redirect(url_for('.admin', exhibit='users'))
                    else:
                        flash(u'error')
                        return redirect(url_for('.admin', exhibit='adduser'))
                else:
                    flash(u'error')
                    return redirect(url_for('.admin', exhibit='adduser'))

            # else:
            #     # print avat.width, avat.height
            #     os.remove('app/static/icons/' + form1.nick.data + '.png')
            #     flash(u'图片格式必须是320px*320px')
            #     return redirect(url_for('.admin', exhibit='adduser'))
        # else:
        #     flash(u'请添加头像')
        #     return redirect(url_for('.admin', exhibit='adduser'))

    #: -----------------edituser------------------#
    form2 = edituser()
    if form2.submitedit.data and form2.validate_on_submit():

        id = str(editid)
        if form2.avataredit.data:

            uid = str(uuid.uuid1())
            avatarurl = "http://oh8c4fk40.bkt.clouddn.com/" + "useravatar_" + uid
            avatarfile = form2.avataredit.data.save('app/static/icons/' + form2.nickedit.data + '.png')
            avat = Image.open('app/static/icons/' + form2.nickedit.data + '.png')
            if avat.width == 320 and avat.height == 320:

                oldavatar = htmltoname(requests.get(host + '/users/' + id, verify=True).json()['avatar'])
                mydelete.mydelete(oldavatar)
                data = {"user": {"phone": form2.phoneedit.data,
                                 "utype": form2.utypeedit.data,

                                 "nick": form2.nickedit.data,
                                 "gender": form2.genderedit.data,
                                 "email": form2.emailedit.data,
                                 "avatar": 'uploading_' + uid + '.png'
                                 }
                        }

                c = requests.post(host + '/users/' + id + '/update', json=data, verify=True)
                myupload.upload("useravatar_" + uid + '.png', 'app/static/icons/' + form2.nickedit.data + '.png')
                data['user']['avatar'] = "useravatar_" + uid + '.png'
                c = requests.post(host + '/users/' + id + '/update', json=data, verify=True)
            else:
                # print avat.width, avat.height
                os.remove('app/static/icons/' + form2.nickedit.data + '.png')
                flash(u'图片格式必须是320px*320px')
                return redirect(url_for('.admin', exhibit='edituser', editid=editid))
        else:
            data = {"user": {"phone": form2.phoneedit.data,
                             "utype": form2.utypeedit.data,

                             "nick": form2.nickedit.data,
                             "gender": form2.genderedit.data,
                             "email": form2.emailedit.data

                             }
                    }

            c = requests.post(host + '/users/' + id + '/update', json=data, verify=True)
        return redirect(url_for('.admin', exhibit='users'))
    if editid != -1:
        form2.phoneedit.data = [i['phone'] for i in users if i['id'] == editid][0]
        form2.nickedit.data = [i['nick'] for i in users if i['id'] == editid][0]
        form2.genderedit.data = str([i['gender'] for i in users if i['id'] == editid][0])
        form2.emailedit.data = [i['email'] for i in users if i['id'] == editid][0]
        form2.utypeedit.data = str([i['utype'] for i in users if i['id'] == editid][0])

    if exhibit=='change_status':
        status = request.form.get('status',type=int)
        if status == 1:
            duration = request.form.get('duration')
            if duration:
                data = { "ban" : { "user_id" : editid, "status" : 1, "duration" : duration } }
                requests.post(host+'/users/change_status',json=data)
                return redirect(url_for('.admin',exhibit='users'))
            else:
                pass
        elif status == 0:
            data = { "ban" : { "user_id" : editid, "status" : 0 } }
            requests.post(host+'/users/change_status',json=data)
            return redirect(url_for('.admin',exhibit='users'))


    #: ----------------deleteuser-----------------#
    if deleteid != -1:
        id = str(deleteid)
        oldavatar = htmltoname(requests.get(host + '/users/' + id, verify=True).json()['avatar'])
        mydelete.mydelete(oldavatar)
        requests.post(host + '/users/' + id + '/delete', verify=True)
        return redirect(url_for('.admin', exhibit='users'))
    return render_template('admin.html', users=users, exhibit=exhibit, form1=form1, form2=form2,upage=upage,
                           trypage=trypage,truepage=truepage,
                    members=members,try_users=try_users,true_users=true_users,try_users2=try_users2,
                           vip_purchases=vip_purchases)

@main.app_template_filter('oddftime')
def _jinja2_filter_odd_datetime(date, fmt=None):
    processed_date=date.split('.')[0]
    time_struct = datetime.strptime(processed_date,'%Y-%m-%dT%H:%M:%S')
    real_time=time_struct + (datetime(2017,5,10,19,0,0)-datetime(2017,5,10,11,0,0))
    return datetime.strftime(real_time,'%Y-%m-%d %H:%M').split(' ')


@main.route('/comments/<exhibit>', methods=['GET', 'POST'])
def comments(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    #: -----------------showcomments----------------#
    commentsresp = requests.get(host + '/comments?page=' + str(page), verify=True)
    comments = commentsresp.json()['records']
    #: -----------------addcomment----------------#
    form1 = addcomment()
    if form1.submit.data and form1.validate_on_submit():
        data = {"comment": { "text": form1.text.data,
                            "status": int(form1.status.data), "poster_id": 1, "lesson_id": 2, "content_id": 3}}
        requests.post(host + '/comments/create', json=data, verify=True)
        return redirect(url_for('.comments', exhibit='show_comments'))
    #: -----------------editcomment----------------#
    form2 = editcomment()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"comment": { "text": form2.textedit.data,
                            "status": int(form2.statusedit.data)}}
        if editid != -1:
            id = str(editid)
            b = requests.post(host + '/comments/' + id + '/update', json=data, verify=True)
            # print b.json()
            return redirect(url_for('.comments', exhibit='show_comments', page=page))
    if editid != -1:
        form2.textedit.data = [i['text'] for i in comments if i['id'] == editid][0]
        form2.statusedit.data = str([i['status'] for i in comments if i['id'] == editid][0])
    #: -----------------deletecomment----------------#
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host + '/comments/' + id + '/delete', verify=True)
        return redirect(url_for('.comments', exhibit='show_comments', page=page))
    return render_template('comments.html', exhibit=exhibit, comments=comments, commentsresp=commentsresp, page=page, \
                           form1=form1, form2=form2)


@main.route('/tags/<exhibit>', methods=['GET', 'POST'])
def tags(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    import random
    page = request.args.get('page', 1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    tagsresp = requests.get(host + '/tags?page=' + str(page), verify=True)
    tags = tagsresp.json()['records']
    tagscloud = [[i['name'], random.randint(50, 300)] for i in tags]

    #: -----------------addtag----------------#
    form1 = addtag()
    if form1.submit.data and form1.validate_on_submit():
        data = {"tag": {"name": form1.name.data}}
        a = requests.post(host + '/tags/create', json=data, verify=True)
        return redirect(url_for('.tags', exhibit='show_tags'))
    #: -----------------edittag----------------#
    form2 = edittag()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"tag": {"name": form2.nameedit.data}}
        if editid != -1:
            id = str(editid)
            a = requests.post(host + '/tags/' + id + '/update', json=data, verify=True)
            return redirect(url_for('.tags', exhibit='show_tags', page=page))
    if editid != -1:
        form2.nameedit.data = [i['name'] for i in tags if i['id'] == editid][0]

    #: -----------------deletetag----------------#
    if deleteid != -1:
        id = str(deleteid)
        a = requests.post(host + '/tags/' + id + '/delete', verify=True)
        return redirect(url_for('.tags', exhibit='show_tags', page=page))
    return render_template('tags.html', exhibit=exhibit, tagsresp=tagsresp, tags=tags, form1=form1, page=page,
                           form2=form2, tagscloud=tagscloud)


@main.route('/consumers/<exhibit>', methods=['GET', 'POST'])
@permission_required(2)
def consumers(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    #: -----------------showconsumers----------------#
    consumersresp = requests.get(host + '/consumers?page=' + str(page), verify=True)
    consumers = consumersresp.json()['records']
    #: -----------------addconsumer----------------#
    form1 = addconsumer()
    if form1.submit.data and form1.validate_on_submit():
        data = {"consumer": {"name": form1.name.data,
                             "status": int(form1.status.data)}}
        requests.post(host + '/consumers/create', json=data, verify=True)
        return redirect(url_for('.consumers', exhibit='show_consumers', page=page))
    #: -----------------editconsumer----------------#
    form2 = editconsumer()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {"consumer": {"name": form2.nameedit.data,
                             "status": int(form2.statusedit.data)}}
        if editid != -1:
            id = str(editid)
            requests.post(host + '/consumers/' + id + '/update', json=data, verify=True)
            return redirect(url_for('.consumers', exhibit='show_consumers', page=page))
    if editid != -1:
        form2.nameedit.data = [i['name'] for i in consumers if i['id'] == editid][0]
        form2.statusedit.data = str([i['status'] for i in consumers if i['id'] == editid][0])

    #: -----------------deleteconsumer----------------#
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host + '/consumers/' + id + '/delete', verify=True)
        return redirect(url_for('.consumers', exhibit='show_consumers', page=page))
    return render_template('consumers.html', exhibit=exhibit, consumersresp=consumersresp, consumers=consumers,
                           page=page,
                           form1=form1, form2=form2)


from .forms import addprice, editprice


@main.route('/prices/<exhibit>', methods=['GET', "POST"])
def prices(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    prices = requests.get(host + '/prices', verify=True).json()
    form1 = addprice()
    if form1.submit.data and form1.validate_on_submit():
        data = {'price': {'price': form1.price.data, 'iap_product_id': form1.iap.data}}
        requests.post(host + '/prices/create', json=data, verify=True)
        return redirect(url_for('.prices', exhibit='show_prices'))
    form2 = editprice()
    if form2.submitedit.data and form2.validate_on_submit():
        data = {'price': {'price': form2.priceedit.data, 'iap_product_id': form2.iapedit.data}}
        if editid != -1:
            id = str(editid)
            requests.post(host + '/prices/' + id + '/update', json=data, verify=True)
            return redirect(url_for('.prices', exhibit='show_prices'))
    if editid != -1:
        form2.priceedit.data = [i['price'] for i in prices if i['id'] == editid][0]
        form2.iapedit.data = [i['iap_product_id'] for i in prices if i['id'] == editid][0]
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host + '/prices/' + id + '/delete', verify=True)
        return redirect(url_for('.prices', exhibit='show_prices'))
    return render_template('prices.html', exhibit=exhibit, form1=form1, form2=form2, prices=prices)


@main.route('/members/<exhibit>', methods=['GET', "POST"])
@permission_required(2)
def members(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    print request.form
    deleteid = request.args.get('deleteid', -1, type=int)
    editid = request.args.get('editid', -1, type=int)
    members_tea = requests.get(host + '/members', verify=True).json()
    members_stu = requests.get(host + '/members?utype=0', verify=True).json()
    members = members_stu + members_tea
    print members
    prices = requests.get(host + '/prices', verify=True).json()
    if request.form.get('mname') and request.form.get('utype_m') and request.form.get('mtype') and request.form.get('mprice') and request.form.get('mtime')  and request.form.get('munit') :
        data = {"member": {"name": request.form.get('mname'),
                           "mtype": request.form.get('mtype'),
                           "utype": request.form.get('utype_m'),
                           "price_id": request.form.get('mprice'),
                           "original_price":request.form.get('ori_price'),
                           "discount_rate":request.form.get('dis_rate'),
                           "period_unit": request.form.get('munit'),
                           "period_amount": request.form.get('mtime')}}
        if exhibit == 'addmember':
            a = requests.post(host + '/members/create', json=data)
            print a.json()
        elif exhibit == 'editmember':
            a = requests.post(host + '/members/' + str(editid) + '/update', json=data)
            print a.json()
        return redirect(url_for('.members', exhibit='show_members'))
    if deleteid != -1:
        id = str(deleteid)
        requests.post(host + '/members/' + id + '/delete', verify=True)
        return redirect(url_for('.members', exhibit='show_members'))

    return render_template('members.html', members=members, exhibit=exhibit, prices=prices)

@main.route('/orders/<exhibit>', methods=['GET', "POST"])
def orders(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    page=request.args.get('page',1,type=int)
    order_no=request.args.get('order_no')
    orderresp=requests.get(host+'/orders?page='+str(page))
    orders = orderresp.json()['records']
    if order_no:
        order_info=requests.get(host+'/orders/wechat/query_order/'+str(order_no)).json()
    else:
        order_info=None
    for i in orders:
        i['order_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(i['order_time']))
        if i['pay_time']:
            i['pay_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(i['pay_time']))

    return render_template('orders.html',exhibit=exhibit,orders=orders,orderresp=orderresp,page=page,order_info=order_info)

@main.route('/permission/<exhibit>', methods=['GET', "POST"])
def permission(exhibit):
    deleteid=request.args.get('deleteid',-1,type=int)
    mtype=request.form.get('mtype')
    lesson=request.form.get('lesson')
    if exhibit=='addpermission':
        members_stu = requests.get(host + '/members?per=0&utype=0', verify=True).json()
        members_tea = requests.get(host + '/members?per=0', verify=True).json()
        members = members_stu +members_tea
        lessons_first=requests.get(host+'/lessons?per=0').json()['records']
        lessons_second=requests.get(host+'/lessons?per=0&aud=1').json()['records']
        lessons=lessons_first+lessons_second
    else:
        members=None
        lessons=None
    if mtype and lesson:
        data={ "member_lesson" : { "member_id" : int(mtype), "lesson_id" : int(lesson)}}
        requests.post(host+'/member_lessons/create',json=data)
        return redirect(url_for('.permission',exhibit='show_permission'))
    if deleteid!=-1:
        requests.post(host+'/member_lessons/'+str(deleteid)+'/delete')
        return redirect(url_for('.permission',exhibit='show_permission'))
    permission=requests.get(host+'/member_lessons').json()
    return render_template('permission.html',exhibit=exhibit,permission=permission,members=members,lessons=lessons)
@main.route('/lessonrelease')
def lessonrelease():
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    release = request.args.getlist('release')
    page = request.args.get('page', 1, type=int)
    lessonresp = requests.get(host + '/lessons?page=' + str(page), verify=True)
    lessons = lessonresp.json()['records']
    if release:
        lessonjson = requests.get(host + '/lessons/' + str(release[0])).json()
        lessonjson['public'] = True if release[1] == '1' else False
        lessonjson.pop('banner')

        a = requests.post(host + '/lessons/' + str(release[0]) + '/update', json=lessonjson, verify=True)
        # print a.json()
        return redirect(url_for('.lessonrelease'))
    return render_template('lessonrelease.html', lessons=lessons, lessonresp=lessonresp, release=release, page=page)
@main.route('/resourcerelease')
def resourcerelease():
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    release = request.args.getlist('release')
    page = request.args.get('page', 1, type=int)
    lessonresp = requests.get(host + '/lessons?aud=1&page=' + str(page), verify=True)
    lessons = lessonresp.json()['records']
    if release:
        lessonjson = requests.get(host + '/lessons/' + str(release[0])).json()
        lessonjson['public'] = True if release[1] == '1' else False
        lessonjson.pop('banner')

        a = requests.post(host + '/lessons/' + str(release[0]) + '/update', json=lessonjson, verify=True)
        # print a.json()
        return redirect(url_for('.resourcerelease'))
    return render_template('resourcerelease.html', lessons=lessons, lessonresp=lessonresp, release=release, page=page)

@main.route('/contentrelease')
def contentrelease():
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    release = request.args.getlist('release')
    page = request.args.get('page', 1, type=int)
    contentresp = requests.get(host + '/contents?page=' + str(page), verify=True)
    contents = contentresp.json()['records']
    if release:
        contentjson = requests.get(host + '/contents/' + str(release[0])).json()
        contentjson['public'] = True if release[1] == '1' else False
        contentjson.pop('banner')
        contentjson.pop('url')
        requests.post(host + '/contents/' + str(release[0]) + '/update', json=contentjson, verify=True)
        return redirect(url_for('.contentrelease'))
    return render_template('contentrelease.html', contents=contents, contentresp=contentresp, release=release,
                           page=page)

@main.route('/records/<exhibit>')
@permission_required(2)
def records(exhibit):
    page = request.args.get('page',1,type=int)
    act = request.args.get('act')
    if act:
        activitiesresp = requests.get(host+'/activities?page='+str(page)+'&act='+act,verify=True)
    else:
        activitiesresp = requests.get(host+'/activities?page='+str(page),verify=True)
    activities = activitiesresp.json()['records']
    return render_template('records.html',exhibit=exhibit,page=page,activities=activities,activitiesresp=activitiesresp)
# @main.route('/favorites/<exhibit>')
# def favorites(exhibit):
#     page = request.args.get('page',1,type=int)

@main.route('/favorites/<exhibit>')
def favorites(exhibit):
    page = request.args.get('page',1,type=int)
    favorite = requests.get(host+'/fav/lessons')
    favorite_records = favorite.json()['records']
    return render_template('favorites.html',exhibit=exhibit,page=page,favorite_records=favorite_records,favorite=favorite)
@main.app_template_filter('strftime')
def _jinja2_filter_datetime(date, fmt=None):
    timeArray = time.localtime(date)
    otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
    return otherStyleTime

@main.route('/feedback/<exhibit>',methods=['GET', "POST"])
def feedback(exhibit):
    page = request.args.get('page',1,type=int)
    if request.method=='POST':
        text = request.form.get('text')
        user = request.form.get('user')
        print user
        data = { "feedback" : { "user_id" : int(user), "is_response" : True, "text" : text } }
        requests.post(host+'/feedbacks/create',json=data)
        return redirect(url_for('.feedback',exhibit=exhibit))
    feedback = requests.get(host+'/feedbacks?page='+str(page))
    feedback_records = feedback.json()['records']

    return render_template('feedback.html',exhibit=exhibit,page=page,feedback_records=feedback_records,feedback=feedback)
@main.route('/finance/<exhibit>',methods=['GET', "POST"])
def finance(exhibit):
    time_from = request.form.get('time_from')
    time_to = request.form.get('time_to')
    if time_from and time_to:
        finance=requests.get(host+'/finance/income?from='+time_from+'&to='+time_to).json()
        student_finance=requests.get(host+'/finance/income?from='+time_from+'&to='+time_to+'&utype=0').json()
        teacher_finance=requests.get(host+'/finance/income?from='+time_from+'&to='+time_to+'&utype=1').json()
    else:
        now=time.strftime("%Y%m%d", time.localtime())
        finance=requests.get(host+'/finance/income?from=20170301&to='+now).json()
        student_finance=requests.get(host+'/finance/income?from=20170301&to='+now+'&utype=0').json()
        teacher_finance=requests.get(host+'/finance/income?from=20170301&to='+now+'&utype=1').json()
    return render_template('finance.html',exhibit=exhibit,finance=finance,student_finance=student_finance,teacher_finance=teacher_finance)
@main.route('/register/<exhibit>', methods=['GET', 'POST'])
def register(exhibit):
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))

    page= request.args.get('page')
    email=request.form.get('email')
    level=request.form.get('level',type=int)
    level_update=request.form.get('level_update',type=int)
    edit_id=request.form.get('edit_id')
    deleteid=request.args.get('deleteid')
    # password=request.form.get('password')
    admins=None
    if exhibit=='show_admin':
        admins=requests.get(host+'/admins').json()
        if deleteid:
            requests.post(host+'/admin/'+deleteid+'/delete')
            return redirect(url_for('main.register',exhibit='show_admin'))
        if level_update!=None and edit_id:
            data={"level":level_update}
            requests.post(host+'/admin/'+edit_id+'/update',json=data)
            return redirect(url_for('main.register',exhibit='show_admin'))
    if email and level!=None:
        data={ "email" : email, "level" : level }
        # data={ "admin" : { "email" : email, "uuid" : uid,
            # 	                "password" : password } }
        a=requests.post(host+'/mail/register_admin',json=data)
        flash(u'发送成功')
        print email,level,a.json(),666
    return render_template('auth/register.html',exhibit=exhibit,admins=admins,page=page)
@main.route('/admin/register', methods=['GET', 'POST'])
def admin_register():
    uid=request.args.get('uuid')
    email=request.args.get('email')
    password=request.form.get('password')
    if uid and email and password:
        data={ "admin" : { "email" : email, "uuid" : uid,
                                "password" : password } }
        a=requests.post(host+'/admin/register',json=data)
        print a.json(),666
        flash(u'操作成功')
        return redirect(url_for('main.index'))
    return render_template('auth/admin_register.html')
@main.route('/editpassword', methods=['GET', 'POST'])
def editpassword():
    if not session.has_key('adminid') or time.time() > json.loads(r.hget('admin', session['adminid']))['refresh_exp']:
        return redirect(url_for('.index'))
    a=requests.post(host+'/mail/modify_password')
    print a.json(),13
    flash(u'发送成功')
    return '<h1>发送成功</h1>'
@main.route('/testeditor')
def editor():
    return render_template('testueditor.html')

@main.route('/user_statistic')
def user_statistic():
    users=requests.get(host+'/users?per=0').json()['all']
    phone_users=requests.get(host+'/users?per=0&has_phone=1').json()['all']
    true_member=requests.get(host+'/users?per=0&mtype=2').json()['all']
    artificial_member_1=requests.get(host+'/users?per=0&mtype=3').json()['all']
    artificial_member_2=requests.get(host+'/users?per=0&mtype=1').json()['all']

    return render_template('user_statistic.html',users=users,phone_users=phone_users,true_member=true_member,artificial_member_1=artificial_member_1,artificial_member_2=artificial_member_2)

@main.route('/upload/', methods=['GET', 'POST'])
def upload():
    action = request.args.get('action')

    # 解析JSON格式的配置文件
    # 这里使用PHP版本自带的config.json文件
    with open(os.path.join('app/static/', 'ueditor', 'php',
                           'config.json')) as fp:
        try:
            # 删除 `/**/` 之间的注释
            CONFIG = json.loads(re.sub(r'\/\*.*\*\/', '', fp.read()))
        except:
            CONFIG = {}

    if action == 'config':
        # 初始化时，返回配置文件给客户端
        result = CONFIG
        print result
        return json.dumps(result)

    if action in ('uploadimage', 'uploadvideo', 'uploadfile'):
        upfile = request.files['upfile']  # 这个表单名称以配置文件为准
        # upfile 为 FileStorage 对象
        # 这里保存文件并返回相应的URL
        # if action=='uploadvideo':
        #     print "存储前",time.time()
        #     vdfile = upfile.save('app/static/upload/112.mp4')
        #     print '存储后',time.time()
        #     uid = str(uuid.uuid1())
        #     baoli = 'http://v.polyv.net/uc/services/rest?method=uploadfile'
        #     print "打开前",time.time()
        #     vdfile = open('app/static/upload/112.mp4','rb')
        #     print "打开后",time.time()
        #     data = {'writetoken':'4eb2be96-fed2-4728-a94d-d8b62ca36670',
        #             'JSONRPC':'{"title":"ddd","tag":"ddd","desc":"ddd"}'
        #             # 'cataid':1484212961756
        #             }
        #     # vdfile = upfile.read()
        #     # if vdfile == vdfile2:
        #     #     print 1
        #     # print vdfile
        #     files =  {'Filedata':vdfile}
        #     # import threading
        #     # t1 = threading.Thread(target=requests.post,kwargs={'url':baoli,'data':data,'files':files})
        #     # t1.setDaemon(True)
        #     # t1.start()
        #     print "发送前",time.time()
        #     a= requests.post(baoli,data=data,files=files).json()
        #     print "发送后",time.time()
        #     print a
        #     content_video_url = a['data'][0]['vid']
        #     vdfile.close()
        #     os.remove('app/static/upload/112.mp4')
        #
        #     #myupload.upload('htmlcontent_'+uid+'.mp4','app/static/upload/112.mp4')
        #     result = {
        #         "state": "SUCCESS",
        #         "url": content_video_url,
        #         "title": "htmlcontent_" + uid+'.mp4',
        #         "original": "https://content.kquestions.com/" + "htmlcontent_" + uid+'.mp4'
        #     }
        #     print "最终",time.time()
        #     print result
        # else:
        upfile.save('app/static/upload/112.jpg')
        uid = str(uuid.uuid1())
        myupload.upload('htmlcontent_' + uid + '.jpg', 'app/static/upload/112.jpg')
        result = {
            "state": "SUCCESS",
            "url": "https://content.kquestions.com/" + "htmlcontent_" + uid + '.jpg' + '?imageView2/2/w/900/interlace/0/q/100',
            "title": "htmlcontent_" + uid + '.jpg',
            "original": "https://content.kquestions.com/" + "htmlcontent_" + uid + '.jpg'
        }

        return json.dumps(result)
        # if action =='uploadvideo':
        #     upfile = request.files['upfile']  # 这个表单名称以配置文件为准
        #     baoli = 'http://v.polyv.net/uc/services/rest?method=uploadfile'
        #
        #     data = {'writetoken':'4eb2be96-fed2-4728-a94d-d8b62ca36670',
        #             'JSONRPC':{'title':'ddd','tag':'ddd','desc':'ddd'},
        #             'Filedata':upfile}
        #     a=requests.post(baoli,data=data)
        #     print a.json()
        #     # upfile 为 FileStorage 对象
        #     # 这里保存文件并返回相应的URL
        #     # upfile.save('app/static/upload/112.jpg')
        #     # uid = str(uuid.uuid1())
        #     # myupload.upload('htmlcontent_'+uid+'.jpg','app/static/upload/112.jpg')
        #     result = {
        #         "state": "SUCCESS",
        #         "url": "https://content.kquestions.com/" + "htmlcontent_" + uid+'.jpg',
        #         "title": "htmlcontent_" + uid+'.jpg',
        #         "original": "https://content.kquestions.com/" + "content_" + uid+'.jpg'
        #     }
        #
        #     return json.dumps(result)


